import 'package:regexpattern/regexpattern.dart';

class Validation{
  // String keySearch = "";
  // final validteController = StreamController<String>();
  static String ValidatePass(String pass){
    if(pass == null){
      return "Password invalid";
    }
    else if(pass.length<6){
      return "Password required 6 characters";
    }
    return null;
  }
  static String validateEmail(String email){
    if(email == null || RegExp(RegexPattern.email).hasMatch(email)==false){
      return "Email invalid";
    }
    return null;
  }
}