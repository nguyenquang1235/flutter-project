import 'package:demo_app_mvvm/login/login_validation.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'login_viewmodel.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Login"),
        ),
        body: BodyState(),
      ),
    );
  }
}

class BodyState extends StatefulWidget {
  @override
  _BodyStateState createState() => _BodyStateState();
}

class _BodyStateState extends State<BodyState> {
  final emailController = TextEditingController();
  final passController = TextEditingController();
  final loginViewModel = new Login_ViewModel();

  @override
  void initState() {
    super.initState();
    emailController.addListener(() {
      loginViewModel.checkValidateEmail(emailController.text);
      loginViewModel.enableLoginBtn();
    });
    passController.addListener(() {
      loginViewModel.checkValidatePass(passController.text);
      loginViewModel.enableLoginBtn();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 40),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          StreamBuilder<String>(
              stream: loginViewModel.emailStream,
              builder: (context, snapshot) {
                return TextFormField(
                  controller: emailController,
                  decoration: InputDecoration(
                      icon: Icon(Icons.email),
                      hintText: "example@gmail.com",
                      labelText: "Email*",
                      errorText: snapshot.data),
                );
              }),
          SizedBox(height: 20),
          StreamBuilder<String>(
              stream: loginViewModel.passStream,
              builder: (context, snapshot) {
                return TextFormField(
                  controller: passController,
                  obscureText: true,
                  decoration: InputDecoration(
                      icon: Icon(Icons.lock),
                      labelText: "Passworld*",
                      errorText: snapshot.data),
                );
              }),
          SizedBox(height: 40),
          SizedBox(
            width: 200,
            height: 45,
            child: StreamBuilder<bool>(
                stream: loginViewModel.btnStream,
                builder: (context, snapshot) {
                  return RaisedButton(
                    color: Colors.blue,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    onPressed: snapshot.data == true
                        ? () {
                            print(
                                "Email: ${emailController.text}\nPass: ${passController.text}");
                          }
                        : null,
                    child: Text(
                      "Login",
                      style: TextStyle(color: Colors.white, fontSize: 17),
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }
}
