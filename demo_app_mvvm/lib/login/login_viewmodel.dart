import 'dart:async';

import 'login_view.dart';
import 'package:async/async.dart';
import 'package:demo_app_mvvm/utility/validation.dart';
import 'package:flutter/foundation.dart';

class Login_ViewModel{

  bool _CheckEmail;
  bool _CheckPass;

  final emailController = StreamController<String>();
  final passController = StreamController<String>();
  final btnController = StreamController<bool>();

  Stream<String> get emailStream => emailController.stream;
  Sink<String> get emailSink => emailController.sink;

  Stream<String> get passStream => passController.stream;
  Sink<String> get passSink => passController.sink;

  Stream<bool> get btnStream => btnController.stream;
  Sink<bool> get btnSink => btnController.sink;

  Login_ViewModel(){
    _CheckPass = false;
    _CheckEmail = false;
  }

  enableLoginBtn(){
    bool enable = _CheckPass && _CheckEmail;
    btnSink.add(enable);
  }

  checkValidatePass(String key){
    var message = Validation.ValidatePass(key);
    if(message != null) {passSink.add(message);}
    else{
      passSink.add(null);
      return _CheckPass = true;
    }
    return _CheckPass = false;
  }

  checkValidateEmail(String key){
    var message = Validation.validateEmail(key);
    if(message != null) {emailSink.add(message);}
    else{
      emailSink.add(null);
      return _CheckEmail = true;
    }
    return _CheckEmail = false;
  }

  dispose(){
    emailController.close();
    passController.close();
    btnController.close();
  }
}