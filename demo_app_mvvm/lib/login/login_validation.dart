import 'dart:async';

import 'package:demo_app_mvvm/utility/validation.dart';

class LoginValidation{
  String keyword = "";
  final validationController = StreamController<String>();



  checkValidatePass(String key){
    var message = Validation.ValidatePass(key);
    if(message != null) {validationController.sink.add(message);}
    else{
      validationController.sink.add(null);
    }
  }

  checkValidateEmail(String key){
    var message = Validation.validateEmail(key);
    if(message != null) {validationController.sink.add(message);}
    else{
      validationController.sink.add(null);
    }
  }

  dispose(){
    validationController.close();
  }
}