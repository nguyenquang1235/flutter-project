import 'package:flutter/material.dart';
import 'package:lichamduong/src/presentation/presentation.dart';
import '../../resource/resource.dart';
import '../base/base.dart';

class SplashViewModel extends BaseViewModel{

  final OtherRepository repository;

  SplashViewModel({@required this.repository});

  init()async{
    final NetworkState<OtherApplication> rs = await repository.getMoreApps();
    final otherApp = rs.data;
    print("Other apps: ${otherApp?.toJson() ?? "Nullllll!"}");
    Navigator.pushNamed(context, Routers.navigation);
  }
}