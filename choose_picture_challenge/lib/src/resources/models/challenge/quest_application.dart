import 'dart:convert';

import 'package:choose_picture_challenge/src/resources/models/challenge/challenge_model.dart';

class QuestApplication {
  final List<Quest> quest;

  QuestApplication({this.quest});

  factory QuestApplication.fromJson(dynamic data) {
    List<Quest> list = [];
    for(var item in data){
      list.add(Quest.fromJson(item));
    }
    return QuestApplication(quest: list);
  }

}
