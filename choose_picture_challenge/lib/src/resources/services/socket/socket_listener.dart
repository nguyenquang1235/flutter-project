class SocketListener{

  void onSocketConnect(dynamic data) {
    print("==========> ON CONNECTED: $data");
  }

  void onSocketDisconnect(dynamic data) {
    print("==========> ON DISCONNECTED: $data");
  }

}