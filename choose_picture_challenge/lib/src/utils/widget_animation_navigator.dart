import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum Animate { Random, Fade, Scale, Rotation }

class WidgetAnimationNavigator extends CupertinoPageRoute {
  final Widget newPage;
  final Animate animate;

  WidgetAnimationNavigator({
    this.newPage,
    this.animate = Animate.Random,
  }) : super(builder: (context) => newPage);

  @override
  bool get maintainState => true;

  @override
  Duration get transitionDuration => Duration(milliseconds: 500);

  @override
  Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
    switch (animate) {
      case Animate.Fade:
        return FadeTransition(opacity: animation, child: newPage);
        break;
      case Animate.Scale:
        return ScaleTransition(scale: animation, child: newPage);
        break;
      case Animate.Rotation:
        return RotationTransition(turns: animation, child: newPage);
        break;
      case Animate.Random:
        int rd = Random.secure().nextInt(3);
        switch (rd) {
          case 1:
            return FadeTransition(opacity: animation, child: newPage);
            break;
          case 2:
            return ScaleTransition(scale: animation, child: newPage);
            break;
          case 3:
            return RotationTransition(turns: animation, child: newPage);
            break;
        }
        break;
    }

    return FadeTransition(opacity: animation, child: newPage);
  }
}
