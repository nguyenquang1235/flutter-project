import 'package:choose_picture_challenge/src/presentation/base/base.dart';
import 'package:choose_picture_challenge/src/presentation/home/home_viewmodel.dart';
import 'package:choose_picture_challenge/src/presentation/routers.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<HomeViewModel>(builder: (context, viewModel, child) {
      return Scaffold(
        body: Container(
          child: Center(
            child: OutlineButton(
              child: Text("Play Game"),
              color: Colors.orange,
              onPressed: () => Navigator.pushNamed(context, Routers.challenge_find_picture),
              // onPressed: () => null,
            ),
          ),
        ),
      );
    }, viewModel: HomeViewModel(),
      onViewModelReady: (viewModel) => viewModel.init(),
    );
  }
}
