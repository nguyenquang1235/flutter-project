import 'package:cached_network_image/cached_network_image.dart';
import 'package:choose_picture_challenge/src/configs/constant/app_end_point.dart';
import 'package:choose_picture_challenge/src/presentation/base/base.dart';
import 'package:choose_picture_challenge/src/resources/resources.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../presentation.dart';

class ChallengeFindPictureViewModel extends BaseViewModel with SocketListener {
  SocketService service;
  QuestApplication questApplication;
  Room room;
  final dynamic state;
  int maxChoice;

  final nextQuestion = BehaviorSubject<bool>();
  final screenSubject = BehaviorSubject<Widget>();
  final roomSubject = BehaviorSubject<Room>();
  final scoreControllerPlayer1 = BehaviorSubject<int>();
  final scoreControllerPlayer2 = BehaviorSubject<int>();

  ChallengeFindPictureViewModel({this.state});

  init() async {
    service = await SocketService.instance();
    service.setListener(this);
    service.listenEvent(SocketService.socket_find_picture_listener);
    maxChoice = 2;
    nextQuestion.sink.add(false);
    onConnect();
  }

  @override
  void dispose() {
    service.offEvent(SocketService.socket_find_picture_listener);
    super.dispose();
  }

  onConnect() async {
    if (await service.isConnected) {
      Future.delayed(Duration(seconds: 2), () => service.emitSameFindPlayer(2));
      screenSubject.sink.add(StreamBuilder(
        stream: roomSubject.stream,
        builder: (context, snapshot) {
          return ChallengeWaitingPlayerScreen(
            room: snapshot.data ?? null,
          );
        },
      ));
    }
  }

  void onSameFindPlayer(data) async {
    room = Room.fromJson(await data);
    roomSubject.sink.add(room);
    scoreControllerPlayer1.sink.add(0);
    scoreControllerPlayer2.sink.add(0);
  }

  void onSamePlayQuestion(data) async {
    questApplication = QuestApplication.fromJson(await data["data"]);
    List<dynamic> list = await data["board"];
    if (questApplication != null) {
      Future.delayed(Duration(seconds: 2),
              () => screenSubject.sink.add(state.buildGameScreen()));
      // Future.delayed(
      //     Duration(milliseconds: 2), () => service.emitSamePlayReady());
    }
  }

  void onSamePlayReady(data) {}

  void onSamePlayShowQuestion(data) {}

  void onSamePlayAnswer(data) {}

  void onSameEmotion(data) {}

  void onSamePlayEndGame(data) {}

  void onSameOutRoom(data) {}

  _resetStream() {
    roomSubject.sink.add(null);
    scoreControllerPlayer1.sink.add(0);
    scoreControllerPlayer2.sink.add(0);
  }
}
