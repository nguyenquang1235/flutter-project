import 'package:choose_picture_challenge/src/configs/configs.dart';
import 'package:choose_picture_challenge/src/configs/constant/app_end_point.dart';
import 'package:choose_picture_challenge/src/presentation/base/base.dart';
import 'package:choose_picture_challenge/src/presentation/challenge_find_picture/challenge_find_picture.dart';
import 'package:choose_picture_challenge/src/presentation/widgets/widget_game_find_image.dart';
import 'package:choose_picture_challenge/src/resources/models/challenge/challenge_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChallengeFindPictureScreen extends StatefulWidget {
  @override
  _ChallengeFindPictureScreenState createState() =>
      _ChallengeFindPictureScreenState();
}

class _ChallengeFindPictureScreenState
    extends State<ChallengeFindPictureScreen> {
  ChallengeFindPictureViewModel _viewModel;
  String compareWord;
  List<WidgetGameFindImage> list = [];
  WidgetGameFindImage obj1;
  WidgetGameFindImage obj2;
  int maxChoice = 2;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ChallengeFindPictureViewModel>(
      onViewModelReady: (viewModel) {
        _viewModel = viewModel;
        _viewModel.init();
      },
      viewModel: ChallengeFindPictureViewModel(state: this),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SafeArea(
            child: StreamBuilder(
              stream: viewModel.screenSubject.stream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return snapshot.data;
                } else {
                  return Container();
                }
              },
            ),
          ),
        );
      },
    );
  }

  buildGameScreen() {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.bgLearn), fit: BoxFit.fill)),
      child: Column(
        children: [
          Expanded(
            child: _buildChallengeHeader(),
            flex: 2,
          ),
          Expanded(
            child: _buildChallengerBody(),
            flex: 5,
          ),
        ],
      ),
    );
  }

  _buildChallengeHeader() {
    return Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.all(4),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildPlayerWaitingInfo(
                url: _viewModel.room.player1.userInfo.avatar,
                name: _viewModel.room.player1.userInfo.fullname,
                color: Colors.transparent,
                min: true,
              ),
              _buildPlayerWaitingInfo(
                url: _viewModel.room.player2.userInfo.avatar,
                name: _viewModel.room.player2.userInfo.fullname,
                color: Colors.transparent,
                min: true,
              ),
            ],
          ),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AppImages.bgChallengeHeader),
                  fit: BoxFit.fill)),
        ),
        Align(
            alignment: Alignment(-0.65, -0.9),
            child: StreamBuilder(
                stream: _viewModel.scoreControllerPlayer1.stream,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Container();
                  }
                  return Text(snapshot.data.toString());
                })),
        Align(
          alignment: Alignment(0.65, -0.9),
          child: StreamBuilder(
            stream: _viewModel.scoreControllerPlayer2.stream,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Container();
              }
              return Text(snapshot.data.toString());
            },
          ),
        ),
      ],
    );
  }

  _buildChallengerBody() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 35),
      child: GridView.count(
        mainAxisSpacing: 8,
        crossAxisSpacing: 8,
        crossAxisCount: 6,
        childAspectRatio: 100 / 70,
        children: _buildGameQuests(),
      ),
    );
  }

  _buildPlayerWaitingInfo({String url, String name, Color color, bool min}) {
    return Container(
      color: color,
      child: Column(
        mainAxisSize: min ? MainAxisSize.min : MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildCircleAvatar(url),
          Text(name),
        ],
      ),
    );
  }

  _buildCircleAvatar(String url) {
    if (url != null) {
      if (!url.contains("https"))
        url = "${AppEndPoint.BASE_SOCKET_URL}:${AppEndPoint.PORT}$url";
    }
    return Container(
      padding: const EdgeInsets.all(2),
      width: 60,
      height: 60,
      child: CircleAvatar(
        backgroundImage: url == null
            ? AssetImage(
                AppImages.icAvatarFind,
              )
            : NetworkImage(
                url,
              ),
      ),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.icBgAvatar), fit: BoxFit.fill),
          borderRadius: BorderRadius.all(Radius.circular(100))),
    );
  }

  _buildGameQuests() {
    for (var item in _viewModel.questApplication.quest) {
      list.add(WidgetGameFindImage(
        question: item,
        isText: false,
      ));
      list.add(WidgetGameFindImage(
        question: item,
        isText: true,
      ));
    }

    list..shuffle();

    for (int i = 0; i < list.length; i++) {
      list[i].callBack = () {
        if (list[i].isFlip) {
          if (obj1 != null && obj2 != null) {
            _checkMaxChoice();
          }
          _onFlipUpImage(list[i]);
        } else if (!list[i].isFlip) {
          _onFlipDownImage(list[i]);
        }
        if (obj1 != null && obj2 != null) {
          compareImage();
        }
      };
    }

    return list;
  }

  _onFlipUpImage(WidgetGameFindImage obj) {
    if (obj1 == null && obj.isFlip) {
      obj1 = obj;
    } else if (obj2 == null && obj.isFlip) {
      obj2 = obj;
    }
    maxChoice--;
  }

  _onFlipDownImage(WidgetGameFindImage obj) {
    if (obj1 == obj) {
      obj1 = null;
    } else {
      obj2 = null;
    }
    maxChoice++;
  }

  _checkMaxChoice() {
    print("vao kiem tra");
    print(maxChoice);
    if (maxChoice <= 0) {
      print("vao day");
      obj1.onFlip();
      obj2.onFlip();
      maxChoice = 2;
    }
  }

  compareImage() async {
    print(obj1.question.word);
    print(obj2.question.word);
    if (obj1 != null && obj2 != null) {
      if (obj1.isFlip && obj2.isFlip && obj1 != obj2) {
        if (obj1.question.word == obj2.question.word) {
          list.where((element) => element == obj1).first.onHide();
          list.where((element) => element == obj2).first.onHide();
          obj1 = null;
          obj2 = null;
        } else {}
      }
    }
  }
}
