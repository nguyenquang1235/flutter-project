import 'package:choose_picture_challenge/src/configs/configs.dart';
import 'package:choose_picture_challenge/src/configs/constant/app_end_point.dart';
import 'package:choose_picture_challenge/src/resources/resources.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class ChallengeWaitingPlayerScreen extends StatefulWidget {
  final Room room;

  const ChallengeWaitingPlayerScreen({Key key, this.room}) : super(key: key);
  @override
  _ChallengeWaitingPlayerScreenState createState() =>
      _ChallengeWaitingPlayerScreenState();
}

class _ChallengeWaitingPlayerScreenState
    extends State<ChallengeWaitingPlayerScreen> {
  int duration = 10;
  final timeController = BehaviorSubject<int>();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage(AppImages.bgFindPlayer), fit: BoxFit.fill),
      ),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: widget.room != null ? _buildPlayerInfo() : _buildFindPlayer(),
    );
  }

  _buildCircleAvatar(String url) {
    if (url != null) {
      if (!url.contains("https"))
        url = "${AppEndPoint.BASE_SOCKET_URL}:${AppEndPoint.PORT}$url";
    }
    return Container(
      padding: const EdgeInsets.all(2),
      width: 60,
      height: 60,
      child: CircleAvatar(
        backgroundImage: url == null
            ? AssetImage(
                AppImages.icAvatarFind,
              )
            : NetworkImage(
                url,
              ),
      ),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.icBgAvatar), fit: BoxFit.fill),
          borderRadius: BorderRadius.all(Radius.circular(100))),
    );
  }

  _buildPlayerWaitingInfo({String url, String name, bool min}) {
    return Container(
      child: Column(
        mainAxisSize: min ? MainAxisSize.min : MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildCircleAvatar(url),
          Text(name ?? ""),
        ],
      ),
    );
  }

  _buildPlayerInfo() {
    return Container(
      child: Row(
        children: [
          Expanded(
            child: _buildPlayerWaitingInfo(
                url: widget.room.player1.userInfo.avatar,
                name: widget.room.player1.userInfo.fullname,
                min: false),
          ),
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              _buildTitle("Đang đợi...", 30, Colors.brown, 4),
              CircularProgressIndicator(
                backgroundColor: Colors.brown.withOpacity(0.5),
                valueColor: AlwaysStoppedAnimation<Color>(
                    Colors.brown.withOpacity(1)
                ),
                strokeWidth: 10,
              ),
            ],
          ),
          Expanded(
            child: _buildPlayerWaitingInfo(
              url: widget.room.player2.userInfo.avatar,
              name: widget.room.player2.userInfo.fullname,
              min: false,
            ),
          )
        ],
      ),
    );
  }

  _buildFindPlayer() {
    duration = 10;
    timeController.sink.add(duration);
    reductionTime();
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildPlayerWaitingInfo(url: null, min: true, name: null),
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              _buildTitle("Đang tìm đối thủ...", 30, Colors.brown, 4),
              StreamBuilder(
                  stream: timeController.stream,
                  builder: (context, snapshot) {
                    return _buildTitle(
                        snapshot.data.toString() ?? duration.toString(), 50, Colors.brown, 7);
                  }),
            ],
          ),
          _buildPlayerWaitingInfo(url: null, min: true, name: null),
        ],
      ),
    );
  }

  reductionTime() async {
    for (int i = duration - 1; i > 0; i--) {
      if (widget.room == null) {
        await Future.delayed(
            Duration(seconds: 1), () => timeController.sink.add(i));
      } else {
        break;
      }
    }
  }

  Widget _buildTitle(String keyTitle, double size, Color color, double stroke) {
    return keyTitle == null
        ? SizedBox()
        : Center(
            child: Stack(
              children: [
                Text(
                  keyTitle,
                  style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(
                    fontSize: size,
                    foreground: Paint()
                      ..style = PaintingStyle.stroke
                      ..strokeWidth = stroke+0.5
                      ..color = color,
                  ),
                ),
                Text(
                  keyTitle,
                  style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(
                    fontSize: size,
                    foreground: Paint()
                      ..style = PaintingStyle.stroke
                      ..strokeWidth = stroke
                      ..color = Colors.white,
                  ),
                ),
                Text(keyTitle,
                    style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(fontSize: size, color: color)),
              ],
            ),
          );
  }
}
