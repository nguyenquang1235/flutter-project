import 'package:cached_network_image/cached_network_image.dart';
import 'package:choose_picture_challenge/src/configs/configs.dart';
import 'package:choose_picture_challenge/src/configs/constant/app_end_point.dart';
import 'package:choose_picture_challenge/src/resources/models/challenge/challenge_model.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

typedef void CallBack();

class WidgetGameFindImage extends StatefulWidget {
  final Quest question;
  final bool isText;
  // final bool nextQuestion;
  CallBack callBack;
  bool get isFlip => createState().isFlip;

  onFlip() => createState().onFlip();
  onHide() => createState().onHide();

  WidgetGameFindImageState widgetGameFindImageState = WidgetGameFindImageState();

  WidgetGameFindImage({Key key, this.question, this.isText}) : super(key: key);
  @override
  WidgetGameFindImageState createState() => widgetGameFindImageState;
}

class WidgetGameFindImageState extends State<WidgetGameFindImage> {
  final widgetController = BehaviorSubject<Widget>();

  Widget answer;
  Widget below;
  Widget image;

  bool isFlip;
  bool isTouch;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    answer = widget.isText ? _buildQuestText() : _buildQuestImage();
    below = _buildCardBelow();
    widgetController.sink.add(_buildCardBelow());
    String url = widget.question.image.replaceAll("public/", "storage/");
    isFlip = false;
    CachedNetworkImage(
      imageUrl: AppEndPoint.BASE_IMAGE + "/" + url,
      fit: BoxFit.fill,
      progressIndicatorBuilder: (context, url, downloadProgress) =>
          CircularProgressIndicator(value: downloadProgress.progress),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: widgetController.stream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          print(snapshot.data);
          return snapshot.data;
        }
        return Container();
      },
    );
  }

  _buildCardBelow() {
    return GestureDetector(
      child: Container(
        child: Image.asset(
          AppImages.icBgCardBelow,
          fit: BoxFit.fill,
        ),
      ),
      onTap: () {
        onFlip();
      },
    );
  }

  _buildQuestImage() {
    String url = widget.question.image.replaceAll("public/", "storage/");
    return GestureDetector(
      child: Container(
        padding: const EdgeInsets.all(4),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AppImages.icBgCardAboveGreen),
                fit: BoxFit.fill)),
        child: Image(image: CachedNetworkImageProvider(AppEndPoint.BASE_IMAGE + "/" + url)),
      ),
      onTap: () {
        onFlip();
      },
    );
  }

  _buildQuestText() {
    return GestureDetector(
      child: Container(
        padding: const EdgeInsets.all(4),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AppImages.icBgCardAboveGreen),
                fit: BoxFit.fill)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              widget.question.word.toUpperCase(),
              style: AppStyles.DEFAULT_SMALL_BOLD,
            ),
            Text(widget.question.spelling)
          ],
        ),
      ),
      onTap: () {
        onFlip();
      },
    );
  }

  onFlip() {
    if(!isFlip){
      widgetController.sink.add(answer);
      isFlip = true;
    }else{
      widgetController.sink.add(below);
      isFlip = false;
    }
    widget.callBack();
  }

  onHide() async {
    Future.delayed(Duration(milliseconds: 500),
            () => widgetController.sink.add(Container()));
  }
}
