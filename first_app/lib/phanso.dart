class PhanSo{
  double TuSo;
  double MauSo;
  PhanSo(double tuSo, double mauSo){
    TuSo = tuSo;
    MauSo = mauSo;
  }

  String Xuat(){
    if(TuSo%MauSo==0) return("${TuSo/MauSo}");
    return("${TuSo}/${MauSo}");
  }

  void ToiGian({double i:5}){
    for(i; i >= 2; i--){
      if(TuSo%i == 0 && MauSo%i == 0){
        TuSo = TuSo/i;
        MauSo = MauSo/i;
        return ToiGian(i:i);
      }
    }
  }
}

class PhepToan{
  double tu1;
  double mau1;
  double tu2;
  double mau2;

  PhepToan(PhanSo1, PhanSo2){
    tu1 = PhanSo1.TuSo;
    mau1 = PhanSo1.MauSo;
    tu2 = PhanSo2.TuSo;
    mau2 = PhanSo2.MauSo;
  }
  PhanSo CongTru() => new PhanSo(tu1*mau2+tu2*mau1, mau1*mau2);
  PhanSo Nhan() => new PhanSo(tu1*tu2, mau1*mau2);
  PhanSo Chia() => new PhanSo(tu1*mau2, tu2*mau1);
}