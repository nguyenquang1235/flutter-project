
import 'package:flutter/material.dart';
import 'src/resource/resource.dart';
import 'package:provider/provider.dart';
void main() {
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(
      create: (_) => BookingViewModel(),
    ),
    ChangeNotifierProvider(
      create: (_) => CustomerManagerProvider(),
    )
  ],child: MyApp(),));
  // MyApp();
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Foody Demo',
      theme: ThemeData(
        // primarySwatch: Colors.green,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: LoginView(),
    );
  }
}

