import 'dart:async';

import 'package:app_foody_demo/src/resource/models/customer_booking.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import '../resource.dart';

class BookingViewModel extends ChangeNotifier {
  final _pageController = BehaviorSubject<PageController>();
  final listSearch = BehaviorSubject<List<CustomerBooking>>();
  // final listSearch = StreamController<CustomerBooking>();
  Day chossenDay;
  Day chossenHour;
  int capacity;
  String level = "1";
  String area = "1";
  bool isMessage;
  Set<TableDraw> ListTable;
  Customer customer;

  BookingViewModel(){
    init();
  }

  PageController get pageController => _pageController.value;

  set pageController(PageController value) {
    _pageController.value = value;
  }

  Stream get listSearchStream => listSearch.stream;
  Sink get listSearchSink => listSearch.sink;

  void init() {
    pageController = PageController(initialPage: 0);
    ListTable = new Set<TableDraw>();
    notifyListeners();
  }

  // chuyen trang

  void backToHome() {
    pageController.jumpToPage(0);
    notifyListeners();
  }

  void nextPage() {
    pageController.nextPage(
        duration: Duration(microseconds: 100), curve: Curves.ease);
    notifyListeners();
  }

  void previousPage() {
    pageController.previousPage(
        duration: Duration(microseconds: 100), curve: Curves.ease);
    notifyListeners();
  }

  bool checkPop() {
    switch (pageController.page.toInt()) {
      case 0:
        return true;
        break;
      case 1:
        pageController.jumpToPage(0);
        return false;
        break;
      case 2:
        pageController.jumpToPage(1);
        return false;
        break;
      default:
        return true;
    }
  }

  String getTable() {
    String result = "";
    var listCompare = ListTable.toList();
    listCompare.sort((a, b) => a.id.compareTo(b.id));
    for (int i = 0; i < ListTable.length; i++) {
      i < ListTable.length - 1
          ? result += "${listCompare[i].id} - "
          : result += "${listCompare[i].id}";
    }
    return result;
  }

  search(String key) {

     var _list = AppSeedingData.LIST_APPOINTMENT_SCHEDULE;
     List<CustomerBooking> _listSearch = new List<CustomerBooking>();

     for (int i = 0; i < _list.length; i++) {
       String code = _list.elementAt(i).codeBooking;
       if (code.contains(key)) {
         _listSearch.add(_list[i]);
       }
     }
     listSearchSink.add(_listSearch);
     notifyListeners();
   }

  @override
  void dispose() {
    _pageController.drain();
    _pageController.close();
    super.dispose();
    notifyListeners();
  }

  void clear() {
    ListTable.clear();
    chossenDay = null;
    chossenHour = null;
    capacity = 0;
    level = "1";
    area = "1";
    notifyListeners();
  }
}
