import 'package:app_foody_demo/src/resource/models/restaurant_data.dart';
import 'package:flutter/material.dart';
import '../resource.dart';

class CustomerManagerProvider extends ChangeNotifier {
  int idRestaurant;
  Day day;
  RestaurantData data;

  CustomerManagerProvider() {
    init();
  }

  void init() {
    idRestaurant = 1;
    day = AppSeedingData.LIST_DAY.last;
    queryRestaurantData();
    notifyListeners();
  }

  void changeIdRestaurant(int value) {
    idRestaurant = value;
    notifyListeners();
  }

  void changeDay(Day value) {
    day = value;
    notifyListeners();
  }

  void queryRestaurantData() {
    data = AppSeedingData.LIST_DATARESTAURANT
        .where((element) =>
            ((element.restaurantId == idRestaurant) && (element.day == day)))
        .first;
  }
}
