import 'package:app_foody_demo/src/resource/views/manager/manager.dart';

import '../../resource.dart';
import 'package:flutter/material.dart';
import 'package:app_foody_demo/src/resource/views/customer-order/customer_order_view.dart';
class RolePageView extends StatefulWidget {
  @override
  _RolePageView createState() => _RolePageView();
}

class _RolePageView extends State<RolePageView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.DEFAULT_INFO,
      // resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      body: RoleBody(),
    );
  }
}
class RoleBody extends StatefulWidget {
  @override
  _RoleBodyState createState() => _RoleBodyState();
}

class _RoleBodyState extends State<RoleBody> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment(0,0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text("CHỌN VAI TRÒ",style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(color: Colors.white,letterSpacing: 1.5),),
          Column(
            children: [
              GestureDetector(
                onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context)=> CustomerBookingHomePage())),
                child: Container(
                  height: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Image.asset(AppImages.CHEF_X1),
                      Text("LỄ TÂN",style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(color: Colors.white,letterSpacing: 1.5),),
                    ],
                  ),
                ),
              ),
              Container(
                height: 150,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Image.asset(AppImages.CHEF_X1),
                    Text("ĐẦU BẾP",style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(color: Colors.white,letterSpacing: 1.5),),
                  ],
                ),
              ),
              GestureDetector(
                onTap:() => Navigator.push(context, MaterialPageRoute(builder: (context)=> ManagerPage())),
                child: Container(
                  height: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Image.asset(AppImages.CHEF_X1),
                      Text("ADMIN",style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(color: Colors.white,letterSpacing: 1.5),),
                    ],
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
