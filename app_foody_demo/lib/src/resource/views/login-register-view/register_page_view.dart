import 'package:flutter/material.dart';
import '../../resource.dart';
import 'login_register_view.dart';

class RegisterView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RegisterPageView();
  }
}

class RegisterPageView extends StatefulWidget {
  @override
  _RegisterPageViewState createState() => _RegisterPageViewState();
}

class _RegisterPageViewState extends State<RegisterPageView> {
  @override
  Widget build(BuildContext context) {
    final AppIconDevices devices = new AppIconDevices(context);
    return Scaffold(
      // resizeToAvoidBottomInset: false,
      // // resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: AppColors.DEFAULT_PRIMARY,
        toolbarHeight: devices.SizeDevice.height / 5,
        centerTitle: true,
        title: Image.asset(devices.Logo),
        automaticallyImplyLeading: false,
      ),
      body: RegisterBody(),
    );
  }
}

class RegisterBody extends StatefulWidget {
  @override
  _RegisterBodyState createState() => _RegisterBodyState();
}

class _RegisterBodyState extends State<RegisterBody> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        LoginBackGround(),
        Container(
          child: RegisterBodyHeader(),
        ),
      ],
    );
  }
}

class RegisterBodyHeader extends StatefulWidget {
  @override
  _RegisterBodyHeaderState createState() => _RegisterBodyHeaderState();
}

class _RegisterBodyHeaderState extends State<RegisterBodyHeader> {
  @override
  Widget build(BuildContext context) {
    final device = AppIconDevices(context);
    return Container(
      height: device.SizeDevice.height * (4 / 5) * (1.15 / 2),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30)),
        color: AppColors.DEFAULT_LIGHT,
      ),
      margin: EdgeInsets.symmetric(horizontal: 15),
      // padding: EdgeInsets.symmetric(horizontal: device.SizeDevice.width*0.07, vertical: device.SizeDevice.height*),
      padding: EdgeInsets.only(
          left: device.SizeDevice.width * 0.07,
          right: device.SizeDevice.width * 0.07,
          top: device.SizeDevice.height * 0.03,
          bottom: device.SizeDevice.height * 0.01),
      child: Column(
        children: [
          SizedBox(
            // height: device.SizeDevice.height*0.05,
            height: 40,
            child: Text(
              "ĐĂNG KÝ",
              style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(letterSpacing: 3),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            "Chúng Tôi Sẽ Gửi Mã Xác Nhận OTP",
            style: AppStyles.DEFAULT_SMALL,
          ),
          Text("Đến Số Điện Thoại Của Bạn Trong Ít Phút",
              style: AppStyles.DEFAULT_SMALL),
          SizedBox(
            height: 10,
          ),
          SizedBox(
            height: 40,
            child: TextFormField(
              textAlignVertical: TextAlignVertical.bottom,
              cursorColor: Colors.green,
              decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  hintText: "Việt Nam (+84)",
                  hintStyle: AppStyles.DEFAULT_MEDIUM,
                  suffixIcon: Padding(
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    child: Icon(
                      Icons.keyboard_arrow_down,
                      color: Colors.grey,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      borderSide: BorderSide(width: 1, color: Colors.white)),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      borderSide: BorderSide(width: 1, color: Colors.white))),
            ),
          ),
          SizedBox(height: 20),
          SizedBox(
            height: 40,
            child: TextFormField(
              textAlignVertical: TextAlignVertical.bottom,
              obscureText: true,
              cursorColor: Colors.green,
              decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  hintText: "Việt Nam (+84)",
                  hintStyle: AppStyles.DEFAULT_SMALL,
                  suffixIcon: Padding(
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    child: Image.asset(AppImages.CHECK_PRIMARY_X1),
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      borderSide: BorderSide(width: 1, color: Colors.white)),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      borderSide: BorderSide(width: 1, color: Colors.white))),
            ),
          ),
          SizedBox(height: 20),
          SizedBox(
            height: 40,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30))),
              color: AppColors.DEFAULT_PRIMARY,
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => OtpPageView()));
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Opacity(
                      opacity: 0, child: Image.asset(AppImages.ICON_ARROW_X1)),
                  Text(
                    "Đăng Ký",
                    textAlign: TextAlign.center,
                    style:
                        AppStyles.DEFAULT_LARGE.copyWith(color: Colors.white),
                  ),
                  Image.asset(AppImages.ICON_ARROW_X1)
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                AppImages.CHECK_PRIMARY_X2,
                width: 20,
                height: 20,
                fit: BoxFit.fill,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "Điều khoản sử dụng",
                style: AppStyles.DEFAULT_MEDIUM
                    .copyWith(decoration: TextDecoration.underline),
              )
            ],
          ),
        ],
      ),
    );
  }
}
