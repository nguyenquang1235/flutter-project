import 'package:app_foody_demo/src/resource/resource.dart';
// import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'login_register_view.dart';

class LoginView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenSaver();
  }
}


class ScreenSaver extends StatefulWidget {
  @override
  _ScreenSaverState createState() => _ScreenSaverState();
}

class _ScreenSaverState extends State<ScreenSaver> {
  @override
  Widget build(BuildContext context) {
    final AppIconDevices devices = new AppIconDevices(context);
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            child: Image.asset(devices.BgGreen,
                width: devices.SizeDevice.width, fit: BoxFit.fill),
          ),
          GestureDetector(
            child: Image.asset(devices.Logo),
            onTap: () => Navigator.push(context,
                MaterialPageRoute(builder: (context) => LoginPageView())),
          )
        ],
      ),
    );
  }
}

class LoginPageView extends StatefulWidget {
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPageView> {
  @override
  Widget build(BuildContext context) {
    final AppIconDevices devices = new AppIconDevices(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      // resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: AppColors.DEFAULT_PRIMARY,
        toolbarHeight: devices.SizeDevice.height / 5,
        centerTitle: true,
        title: Image.asset(devices.Logo),
        automaticallyImplyLeading: false,
      ),
      body: LoginBody(),
    );
  }
}

class LoginBody extends StatefulWidget {
  @override
  _LoginBodyState createState() => _LoginBodyState();
}

class _LoginBodyState extends State<LoginBody> {
  @override
  Widget build(BuildContext context) {
    final AppIconDevices devices = new AppIconDevices(context);
    return Stack(
      alignment: Alignment(0, 0.06),
      children: [
        LoginBackGround(),
        Container(
          height: devices.SizeDevice.height * 4 / 5,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: [
              LoginBodyHeader(),
              LoginFooter(),
            ],
          ),
        ),
        Image.asset(devices.FingerPrint)
      ],
    );
  }
}

class LoginBackGround extends StatefulWidget {
  @override
  _LoginBackGroundState createState() => _LoginBackGroundState();
}

class _LoginBackGroundState extends State<LoginBackGround> {
  @override
  Widget build(BuildContext context) {
    final AppIconDevices devices = new AppIconDevices(context);
    return Container(
      child: Image.asset(
        devices.Component,
        width: devices.SizeDevice.width,
        height: devices.SizeDevice.height * (4 / 5),
        fit: BoxFit.fill,
      ),
    );
  }
}

class LoginBodyHeader extends StatefulWidget {
  @override
  _LoginBodyHeaderState createState() => _LoginBodyHeaderState();
}

class _LoginBodyHeaderState extends State<LoginBodyHeader> {
  @override
  Widget build(BuildContext context) {
    final device = AppIconDevices(context);
    return Container(
      height: device.SizeDevice.height * (4 / 5) * (1 / 2),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30)),
        color: AppColors.DEFAULT_LIGHT,
      ),
      margin: EdgeInsets.symmetric(horizontal: 15),
      // padding: EdgeInsets.symmetric(horizontal: device.SizeDevice.width*0.07, vertical: device.SizeDevice.height*),
      padding: EdgeInsets.only(
          left: device.SizeDevice.width * 0.07,
          right: device.SizeDevice.width * 0.07,
          top: device.SizeDevice.height * 0.03,
          bottom: device.SizeDevice.height * 0.05),
      child: Column(
        children: [
          SizedBox(
            // height: device.SizeDevice.height*0.05,
            height: 40,
            child: Text(
              "ĐĂNG NHẬP",
              style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(letterSpacing: 3),
            ),
          ),
          SizedBox(
            height: 40,
            child: TextFormField(
              textAlignVertical: TextAlignVertical.bottom,
              cursorColor: Colors.green,
              decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  hintText: "Tài Khoản/Số Điện Thoại",
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      borderSide: BorderSide(width: 1, color: Colors.white)),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      borderSide: BorderSide(width: 1, color: Colors.white))),
            ),
          ),
          SizedBox(height: 20),
          SizedBox(
            height: 40,
            child: TextFormField(
              textAlignVertical: TextAlignVertical.bottom,
              obscureText: true,
              cursorColor: Colors.green,
              decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  hintText: "Mật Khẩu",
                  suffixIcon: Padding(
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    child: Image.asset(AppImages.ICON_VIEW_EYE_X1),
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      borderSide: BorderSide(width: 1, color: Colors.white)),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      borderSide: BorderSide(width: 1, color: Colors.white))),
            ),
          ),
          SizedBox(
            height: 40,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Image.asset(AppImages.CHECK_PRIMARY_X1),
                    Text(" Lưu Đăng Nhập")
                  ],
                ),
                GestureDetector(
                  child: Text("Quên Mật Khẩu?"),
                  onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FogotPassPageView())),
                )
              ],
            ),
          ),
          SizedBox(
            height: 40,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30))),
              color: AppColors.DEFAULT_PRIMARY,
              onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context)=>RolePageView())),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Opacity(
                      opacity: 0, child: Image.asset(AppImages.ICON_ARROW_X1)),
                  Text(
                    "Đăng Nhập",
                    textAlign: TextAlign.center,
                    style:
                        AppStyles.DEFAULT_LARGE.copyWith(color: Colors.white),
                  ),
                  Image.asset(AppImages.ICON_ARROW_X1)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class LoginFooter extends StatefulWidget {
  @override
  _LoginFooterState createState() => _LoginFooterState();
}

class _LoginFooterState extends State<LoginFooter> {
  @override
  Widget build(BuildContext context) {
    final device = AppIconDevices(context);
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
          boxShadow: [],
          color: AppColors.DEFAULT_LIGHT.withOpacity(0.75),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30), topRight: Radius.circular(30))),
      padding: EdgeInsets.symmetric(
          horizontal: device.SizeDevice.width * 0.07,
          vertical: device.SizeDevice.height * 0.02),
      child: Column(
        children: [
          SizedBox(
              child: Text(
            "Đăng Nhập Bằng Tài Khoản Liên Kết",
            style: AppStyles.DEFAULT_SMALL,
          )),
          SizedBox(
            height: device.SizeDevice.height * 0.1,
            width: 200,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Image.asset(AppImages.FACEBOOK_X1),
                Image.asset(AppImages.ZALO_X1),
                Image.asset(AppImages.GROUP_X1),
              ],
            ),
          ),
          SizedBox(
            height: device.SizeDevice.height * 0.05,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Chưa có tài khoản?"),
                GestureDetector(
                  child: Text(
                    "Đăng kí ngay",
                    style: AppStyles.DEFAULT_MEDIUM_BOLD
                        .copyWith(decoration: TextDecoration.underline),
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RegisterView()));
                  },
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
