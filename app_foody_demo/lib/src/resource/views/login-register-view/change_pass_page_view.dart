import 'package:flutter/material.dart';
import '../../resource.dart';
import 'login_register_view.dart';

class ChangePassPageView extends StatefulWidget {
  @override
  _ChangePassPageViewState createState() => _ChangePassPageViewState();
}

class _ChangePassPageViewState extends State<ChangePassPageView> {
  @override
  Widget build(BuildContext context) {
    final AppIconDevices devices = new AppIconDevices(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      // // resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: AppColors.DEFAULT_PRIMARY,
        toolbarHeight: devices.SizeDevice.height / 5,
        centerTitle: true,
        title: Image.asset(devices.Logo),
        automaticallyImplyLeading: false,
      ),
      body: ChangePassBody(),
    );
  }
}

class ChangePassBody extends StatefulWidget {
  @override
  _ChangePassBodyState createState() => _ChangePassBodyState();
}

class _ChangePassBodyState extends State<ChangePassBody> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        LoginBackGround(),
        Container(
          child: ChangePassBodyHeader(),
        ),
      ],
    );
  }
}

class ChangePassBodyHeader extends StatefulWidget {
  @override
  _ChangePassBodyHeaderState createState() => _ChangePassBodyHeaderState();
}

class _ChangePassBodyHeaderState extends State<ChangePassBodyHeader> {
  @override
  Widget build(BuildContext context) {
    final device = AppIconDevices(context);
    return Container(
      height: device.SizeDevice.height * (4 / 5) * (1.15 / 2),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30)),
        color: AppColors.DEFAULT_LIGHT,
      ),
      margin: EdgeInsets.symmetric(horizontal: 15),
      // padding: EdgeInsets.symmetric(horizontal: device.SizeDevice.width*0.07, vertical: device.SizeDevice.height*),
      padding: EdgeInsets.only(
          left: device.SizeDevice.width * 0.07,
          right: device.SizeDevice.width * 0.07,
          top: device.SizeDevice.height * 0.03,
          bottom: device.SizeDevice.height * 0.01),
      child: Column(
        children: [
          SizedBox(
            // height: device.SizeDevice.height*0.05,
            child: Text(
              "THAY ĐỔI MẬT KHẨU",
              style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(letterSpacing: 3),
            ),
          ),
          SizedBox(
            height: 40,
          ),
          Text(
            "Vui Lòng Nhập Mật Khẩu Mới",
            style: AppStyles.DEFAULT_SMALL,
          ),
          Text("Và Xác Nhận Mật Khẩu Mới Vào Ô Bên Dưới",
              style: AppStyles.DEFAULT_SMALL),
          SizedBox(
            height: 50,
          ),
          SizedBox(
            height: 40,
            child: TextFormField(
              obscureText: true,
              textAlignVertical: TextAlignVertical.bottom,
              cursorColor: Colors.green,
              decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  hintText: "Mật Khẩu mới",
                  hintStyle: AppStyles.DEFAULT_MEDIUM,
                  suffixIcon: Padding(
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    child: Image.asset(AppImages.CHECK_PRIMARY_X1),
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      borderSide: BorderSide(width: 1, color: Colors.white)),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      borderSide: BorderSide(width: 1, color: Colors.white))),
            ),
          ),
          SizedBox(height: 20),
          SizedBox(
            height: 40,
            child: TextFormField(
              textAlignVertical: TextAlignVertical.bottom,
              obscureText: true,
              cursorColor: Colors.green,
              decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  hintText: "Nhập Lại Mật Khẩu Mới",
                  hintStyle: AppStyles.DEFAULT_SMALL,
                  suffixIcon: Padding(
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    child: Image.asset(AppImages.CHECK_PRIMARY_X1),
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      borderSide: BorderSide(width: 1, color: Colors.white)),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      borderSide: BorderSide(width: 1, color: Colors.white))),
            ),
          ),
          SizedBox(height: 10),
          SizedBox(
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30))),
              color: AppColors.DEFAULT_PRIMARY,
              onPressed: () => null,
              // onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context)=>ChangePassPageView())),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Opacity(
                      opacity: 0, child: Image.asset(AppImages.ICON_ARROW_X1)),
                  Text(
                    "ĐĂNG NHẬP",
                    textAlign: TextAlign.center,
                    style:
                    AppStyles.DEFAULT_LARGE.copyWith(color: Colors.white),
                  ),
                  Image.asset(AppImages.ICON_ARROW_X1)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

