import 'package:flutter/material.dart';
import '../../resource.dart';
import "login_register_view.dart";
import 'package:pinput/pin_put/pin_put.dart';


class OtpPageView extends StatefulWidget {
  @override
  _OtpPageViewState createState() => _OtpPageViewState();
}


class _OtpPageViewState extends State<OtpPageView> {
  @override
  Widget build(BuildContext context) {
    final AppIconDevices devices = new AppIconDevices(context);
    return Scaffold(
      // resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: AppColors.DEFAULT_PRIMARY,
        toolbarHeight: devices.SizeDevice.height / 5,
        centerTitle: true,
        title: Image.asset(devices.Logo),
        automaticallyImplyLeading: false,
      ),
      body: OtpBody(),
    );
  }
}

class OtpBody extends StatefulWidget {
  @override
  _OtpBodyState createState() => _OtpBodyState();
}

class _OtpBodyState extends State<OtpBody> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        LoginBackGround(),
        Container(
          child: OtpBodyHeader(),
        ),
      ],
    );
  }
}

class OtpBodyHeader extends StatefulWidget {
  @override
  _OtpBodyHeaderState createState() => _OtpBodyHeaderState();
}

class _OtpBodyHeaderState extends State<OtpBodyHeader> {
  final otpController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final device = AppIconDevices(context);
    return Container(
      width: device.SizeDevice.width,
      height: device.SizeDevice.height * (4 / 5) * (1.15 / 2),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30)),
        color: AppColors.DEFAULT_LIGHT,
      ),
      margin: EdgeInsets.symmetric(horizontal: 15),
      // padding: EdgeInsets.symmetric(horizontal: device.SizeDevice.width*0.07, vertical: device.SizeDevice.height*),
      padding: EdgeInsets.only(
          left: device.SizeDevice.width * 0.07,
          right: device.SizeDevice.width * 0.07,
          top: device.SizeDevice.height * 0.03,
          bottom: device.SizeDevice.height * 0.01),
      child: Column(
        children: [
          SizedBox(
            // height: device.SizeDevice.height*0.05,
            child: Text(
              "NHẬP MÃ OTP",
              style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(letterSpacing: 3),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            "Xin Mời Bạn Nhập Mã OTP Chúng Tôi",
            style: AppStyles.DEFAULT_SMALL,
          ),
          Text("Đã Gửi Đên Số +84 9 33 666 666",
              style: AppStyles.DEFAULT_SMALL),
          SizedBox(
            height: 40,
          ),
          SizedBox(
            width: 225,
            child: PinPut(
              controller: otpController,
              fieldsCount: 4,
              keyboardType: TextInputType.number,
              textStyle: TextStyle(
                color: Colors.green,
                fontSize: 24,
                fontWeight: FontWeight.w600,
              ),
              autovalidateMode: AutovalidateMode.onUserInteraction,
              eachFieldHeight: 60,
              eachFieldWidth: 45,
              pinAnimationType: PinAnimationType.fade,
              followingFieldDecoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      offset: Offset(0, 0),
                      blurRadius: 6,
                    )
                  ]),
              selectedFieldDecoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  border: Border.all(color: AppColors.DEFAULT_PRIMARY, width: 2)),
              submittedFieldDecoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          SizedBox(
            child: GestureDetector(
              child: Text(
                "GỬI LẠI MÃ OTP",
                textAlign: TextAlign.center,
                style: AppStyles.DEFAULT_MEDIUM_BOLD
                    .copyWith(color: AppColors.DEFAULT_PRIMARY),
              ),
            ),
          ),
          SizedBox(
            height: 40,
          ),
          Text("OTP Có Hiệu Lực Trong 00:32",
              style: AppStyles.DEFAULT_MEDIUM
                  .copyWith(color: AppColors.DEFAULT_DANGER))
        ],
      ),
    );
  }
}
