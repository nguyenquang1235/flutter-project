
import 'package:flutter/material.dart';
import 'customer_order_view.dart';
import 'package:app_foody_demo/src/resource/viewmodels/viewmodels.dart';
import 'package:provider/provider.dart';


class BookingPage extends StatefulWidget {
  @override
  _BookingPageState createState() => _BookingPageState();
}

class _BookingPageState extends State<BookingPage> {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<BookingViewModel>(context, listen: false);
    // TODO: implement build
    return Container(
      child: PageView(
        physics: const NeverScrollableScrollPhysics(),
        controller: provider.pageController,
        children: [
          BookingHome(providerNextPage: () => provider.nextPage(),),
          BookingBasicInfomation(providerNextPage: () => provider.nextPage(),),
          BookingTable(providerNextPage: () => provider.nextPage(),),
          BookingResult(backHomePage: () => provider.backToHome(),)
        ],
      ),
    );
  }
}

// class BookingPage extends StatefulWidget {
//   @override
//   _BookingPageState createState() => _BookingPageState();
// }
//
// class _BookingPageState extends State<BookingPage> {
//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return Provider<BookingViewModel>(
//       create: (context) => BookingViewModel(),
//       child: BookingBody());
//   }
// }
//
// class BookingBody extends StatefulWidget {
//   @override
//   _BookingBodyState createState() => _BookingBodyState();
// }
//
// class _BookingBodyState extends State<BookingBody> {
//   @override
//   Widget build(BuildContext context) {
//     final provider = Provider.of<BookingViewModel>(context, listen: false);
//     // TODO: implement build
//     return Container(
//       child: PageView(
//         physics: const NeverScrollableScrollPhysics(),
//         controller: provider.pageController,
//         children: [
//           BookingHome(providerNextPage: () => provider.nextPage(),),
//           BookingBasicInfomation(providerNextPage: () => provider.nextPage(),),
//           BookingTable(providerNextPage: () => provider.nextPage(),),
//           BookingResult(backHomePage: () => provider.backToHome(),)
//         ],
//       ),
//     );
//   }
// }

