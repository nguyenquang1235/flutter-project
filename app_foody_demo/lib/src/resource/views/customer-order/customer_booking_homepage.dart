import 'package:flutter/material.dart';

import '../../resource.dart';
import 'customer_order_view.dart';


class CustomerBookingHomePage extends StatefulWidget {
  @override
  _CustomerBookingHomePageState createState() =>
      _CustomerBookingHomePageState();
}

class _CustomerBookingHomePageState extends State<CustomerBookingHomePage> with SingleTickerProviderStateMixin {
  TabController tabcontroller;

  @override
  void initState() {
    super.initState();
    tabcontroller = TabController(length: 4, vsync: this);
  }


  @override
  void dispose() {
    tabcontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: Stack(
          children: [
            Column(
              children: [
                Expanded(
                  flex: 1,
                  child: Container(
                    height: 70,
                    padding: EdgeInsets.only(bottom: 20, left: 10, right: 10),
                    color: AppColors.DEFAULT_PRIMARY,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Image.asset(AppImages.ICON_BACK_ARROW_X1),
                        Image.asset(AppImages.ICON_MENU_X1)
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 120,
                    decoration: BoxDecoration(
                        color: AppColors.DEFAULT_DARK,
                        borderRadius:
                        BorderRadius.vertical(bottom: Radius.circular(10))),
                    child: TabBar(
                      controller: tabcontroller,
                      labelPadding:
                      EdgeInsets.only(left: 15, right: 15, top: 16),
                      isScrollable: true,
                      indicator: UnderlineTabIndicator(
                          insets: EdgeInsets.only(
                              bottom: 20, left: 30, right: 30),
                          borderSide: BorderSide(
                              color: AppColors.DEFAULT_WARRING, width: 5)),
                      labelColor: AppColors.DEFAULT_WARRING,
                      labelStyle: AppStyles.DEFAULT_MEDIUM_BOLD,
                      unselectedLabelColor: Colors.white,
                      unselectedLabelStyle: AppStyles.DEFAULT_SMALL_BOLD,
                      tabs: [
                        Tab(
                          text: "ĐẶT BÀN",
                        ),
                        Tab(
                          text: "GỌI MÓN",
                        ),
                        Tab(
                          text: "PHỤC VỤ",
                        ),
                        Tab(
                          text: "THANH TOÁN",
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 12,
                  child: TabBarView(
                    controller: tabcontroller,
                    children: [
                      // CustomerChoseDay(),
                      BookingPage(),
                      Center(child: Text("Gọi Món")),
                      Center(child: Text("Phục Vụ")),
                      Center(child: Text("Thanh Toán"))
                    ],
                  ),
                ),
              ],
            ),
            Align(
                child: Image.asset(AppImages.CHEF_X1),
              alignment: Alignment(0,-0.95),
            )
          ],
        ),
      ),
    );
  }
}
