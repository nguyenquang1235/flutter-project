import 'dart:math';

import 'package:app_foody_demo/src/resource/resource.dart';

import 'customer_order_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BookingResult extends StatefulWidget {
  final ProviderCall backHomePage;
  @override
  _BookingResultState createState() => _BookingResultState();
  BookingResult({this.backHomePage});
}

class _BookingResultState extends State<BookingResult> {
  BookingViewModel provider;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    provider = Provider.of<BookingViewModel>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Expanded(
            child: buildBookingInfo(),
            flex: 4,
          ),
          Expanded(
            child: buildCustomerInfomation(),
            flex: 8,
          )
        ],
      ),
    );
  }

  Widget buildBookingInfo() {
    return Container(
      padding: EdgeInsets.only(top: 24, bottom: 12, left: 24, right: 24),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "LỊCH HẸN ĐÃ ĐẶT",
            style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(letterSpacing: 2),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                        "${provider.chossenDay.DayNumber}/${provider.chossenDay.DayInWeek}",
                        style: AppStyles.DEFAULT_MEDIUM_BOLD),
                    Text("Ngày Hẹn")
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    Text(
                        "${provider.chossenHour.DayNumber}:${provider.chossenHour.DayInWeek}",
                        style: AppStyles.DEFAULT_MEDIUM_BOLD),
                    Text("Giờ Hẹn")
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text("${provider.capacity}",
                        style: AppStyles.DEFAULT_MEDIUM_BOLD),
                    Text("Chỗ")
                  ],
                ),
              ),
            ],
          ),
          Divider(
            height: 2,
            color: Colors.black,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${provider.getTable()}",
                      style: AppStyles.DEFAULT_MEDIUM_BOLD,
                    ),
                    Text("Bàn Số")
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    Text("${provider.area}",
                        style: AppStyles.DEFAULT_MEDIUM_BOLD),
                    Text("Khu Vực")
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text("${provider.level}",
                        style: AppStyles.DEFAULT_MEDIUM_BOLD),
                    Text("Tầng")
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget buildCustomerInfomation() {
    TextStyle medium =
        AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.white);
    TextStyle small = AppStyles.DEFAULT_SMALL.copyWith(color: Colors.white);
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      height: 331,
      color: AppColors.DEFAULT_DARK,
      child: Column(children: [
        Expanded(
          flex: 2,
          child: Text(
            "THÔNG TIN KHÁCH HÀNG",
            style: AppStyles.DEFAULT_LARGE
                .copyWith(color: Colors.white, letterSpacing: 2),
          ),
        ),
        Expanded(
          flex: 14,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${provider.customer.name}",
                        style: medium,
                      ),
                      Text(
                        "Họ Và Tên",
                        style: small,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${provider.customer.phoneNumber}",
                        style: medium,
                      ),
                      Text(
                        "Số Điện Thoại",
                        style: small,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        provider.customer.birth == null
                            ? "Trống"
                            : "${provider.customer.birth.year}/${provider.customer.birth.month}/${provider.customer.birth.day}",
                        style: medium,
                      ),
                      Text(
                        "Sinh Nhật",
                        style: small,
                      )
                    ],
                  )
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        "${Random(10000).nextInt(100000)}",
                        style: medium,
                      ),
                      Text(
                        "Mã Đặt Chỗ",
                        style: small,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        "${provider.capacity}",
                        style: medium,
                      ),
                      Text(
                        "Số Khách Sẽ Đến",
                        style: small,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        (provider.isMessage) ? "Đã Gửi" : "Chưa Gửi",
                        style: medium,
                      ),
                      Text(
                        "SMS",
                        style: small,
                      )
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
        Expanded(
          flex: 2,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30))),
              color: AppColors.DEFAULT_PRIMARY,
              onPressed: () {
                Provider.of<BookingViewModel>(context, listen: false).clear();
                widget.backHomePage();
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Transform.rotate(
                    child: Image.asset(AppImages.ICON_ARROW_X1),
                    angle: pi,
                  ),
                  Text(
                    "QUAY LẠI MÀN HÌNH CHÍNH",
                    textAlign: TextAlign.center,
                    style:
                        AppStyles.DEFAULT_LARGE.copyWith(color: Colors.white),
                  ),
                  Opacity(
                    child: Image.asset(AppImages.ICON_ARROW_X1),
                    opacity: 0,
                  )
                ],
              ),
            ),
          ),
        ),
        Expanded(
          child: Opacity(
            opacity: 0,
            child: Text("Adasdasdasd"),
          ),
          flex: 1,
        )
      ]),
    );
  }
}
