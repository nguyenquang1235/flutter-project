import 'dart:math';

import 'package:app_foody_demo/src/resource/models/customer_booking.dart';
import 'package:app_foody_demo/src/resource/resource.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:loading_animations/loading_animations.dart';

class BookingHome extends StatefulWidget {
  final ProviderCall providerNextPage;
  @override
  _BookingHomeState createState() => _BookingHomeState();
  BookingHome({this.providerNextPage});
}

class _BookingHomeState extends State<BookingHome>
    with TickerProviderStateMixin {
  TabController tabcontroller;
  TextEditingController _searchQuery;
  var appointment;
  var listable;
  var provider;
  List<CustomerBooking> list;
  @override
  void initState() {
    super.initState();
    provider = Provider.of<BookingViewModel>(context, listen: false);
    tabcontroller = TabController(length: 2, vsync: this);
    _searchQuery = new TextEditingController();
    listable = AppSeedingData.LIST_TABLE;
    appointment = AppSeedingData.LIST_APPOINTMENT_SCHEDULE;
    // list = new List<CustomerBooking>();
    _searchQuery.addListener(() {
      provider.search(_searchQuery.text);
    });
  }

  @override
  void dispose() {
    tabcontroller.dispose();
    _searchQuery.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: buildManagementHeader(),
          flex: 4,
        ),
        Expanded(
          child: _searchQuery.text.isEmpty
              ? buildManagementBody()
              : searchModule(),
          flex: 15,
        ),
      ],
    );
  }

  Widget searchModule() {
    return Container(
      child: Column(
        children: [
          Text(
            "KẾT QUẢ TÌM KIẾM",
            style: AppStyles.DEFAULT_LARGE_BOLD
                .copyWith(color: AppColors.DEFAULT_DARK),
          ),
          buildFooterBookingLocation(),
          Expanded(
            child: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(AppImages.COMPONENT_X1),
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                searchResult(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget searchResult() {
    return Container(
      margin: EdgeInsets.only(top: 12, left: 12, right: 12),
      padding: EdgeInsets.symmetric(vertical: 12),
      decoration: BoxDecoration(
        color: AppColors.DEFAULT_DARK.withOpacity(0.8),
        borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
      ),
      child: StreamBuilder<List<CustomerBooking>>(
        stream: provider.listSearchStream,
        builder: (context, snapshot) {
          var data = snapshot.data;
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: LoadingBouncingLine.circle(
                size: 100,
                backgroundColor: Colors.white,
              ),
            );
          }
          else{
            return ListView.builder(
              itemCount: data.length,
              itemBuilder: (context, index) {
                return buildRowSchedule(customerBooking: data[index]);
              },
            );
          }
        },
      ),
    );
  }

  Widget buildManagementHeader() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 24),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: [
          buildButtonCreateNewBooking(),
          CustomSizeBox(
            _searchQuery,
            "Nhập Mã Đặt Chỗ",
            true,
            height: 40,
            surfix: Icon(
              Icons.book,
              color: Colors.grey,
            ),
          ),
        ],
      ),
    );
  }

  Widget buildButtonCreateNewBooking() {
    return RaisedButton(
      color: AppColors.DEFAULT_PRIMARY,
      onPressed: () => widget.providerNextPage(),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(30))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Opacity(opacity: 0, child: Image.asset(AppImages.ICON_ARROW_X1)),
          Text(
            "TẠO LỊCH HẸN MỚI",
            style: AppStyles.DEFAULT_LARGE.copyWith(color: Colors.white),
          ),
          Image.asset(AppImages.ICON_ARROW_X1),
        ],
      ),
    );
  }

  Widget buildManagementBody() {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Expanded(
            flex: 7,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomSizeBox(
                    null,
                    "Tất Cả Các Khu",
                    true,
                    height: 40,
                    surfix: Icon(
                      Icons.keyboard_arrow_down,
                      color: Colors.grey,
                    ),
                  ),
                  CustomSizeBox(
                    null,
                    "Tầng 1-3",
                    true,
                    height: 40,
                    surfix: Icon(
                      Icons.keyboard_arrow_down,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: buildTabBar(),
            flex: 4,
          ),
          Expanded(
            child: buildFooterBookingLocation(),
            flex: 3,
          ),
          Expanded(
            child: buildTabView(),
            flex: 20,
          )
        ],
      ),
    );
  }

  Widget buildTabBar() {
    return TabBar(
      controller: tabcontroller,
      labelPadding: EdgeInsets.only(left: 9.5, right: 9.5),
      isScrollable: true,
      indicator: UnderlineTabIndicator(
          insets: EdgeInsets.only(left: 40, right: 40),
          borderSide: BorderSide(color: AppColors.DEFAULT_DANGER, width: 5)),
      labelColor: AppColors.DEFAULT_DANGER,
      labelStyle: AppStyles.DEFAULT_LARGE_BOLD,
      unselectedLabelColor: AppColors.DEFAULT_DARK,
      unselectedLabelStyle: AppStyles.DEFAULT_MEDIUM_BOLD,
      tabs: [
        Tab(
          text: "TÌNH TRẠNG BOOKING",
        ),
        Tab(
          text: "LỊCH HẸN",
        )
      ],
    );
  }

  Widget buildFooterBookingLocation() {
    return Container(
      padding: EdgeInsets.only(left: 18, right: 18),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Container(
                width: 16,
                height: 16,
                child: CircleAvatar(
                  backgroundColor: AppColors.DEFAULT_PRIMARY,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "Đã Tới Giờ Hen",
                style: AppStyles.DEFAULT_SMALL,
              )
            ],
          ),
          Row(
            children: [
              Container(
                width: 16,
                height: 16,
                child: CircleAvatar(
                  backgroundColor: AppColors.DEFAULT_WARRING,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "Sắp Tới",
                style: AppStyles.DEFAULT_SMALL,
              )
            ],
          ),
          Row(
            children: [
              Container(
                width: 16,
                height: 16,
                child: CircleAvatar(
                  backgroundColor: AppColors.DEFAULT_DANGER,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "Đã Hủy",
                style: AppStyles.DEFAULT_SMALL,
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget buildTabView() {
    return Column(
      children: [
        Expanded(
          child: TabBarView(
            controller: tabcontroller,
            children: [
              buildGridViewTable(),
              Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(AppImages.COMPONENT_X1),
                      fit: BoxFit.fill),
                ),
                child: buildAppointmentSchedule(),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget buildGridViewTable() {
    return Container(
      color: AppColors.DEFAULT_DARK,
      child: Column(
        children: [
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: Transform.rotate(
                    angle: -pi / 24,
                    child: Divider(
                      color: Colors.white,
                      thickness: 3,
                    ),
                  ),
                ),
                Expanded(
                  child: Transform.rotate(
                    angle: pi / 24,
                    child: Divider(
                      color: Colors.white,
                      thickness: 3,
                    ),
                  ),
                )
              ],
            ),
            flex: 3,
          ),
          Expanded(
            child: Text("Khu 1 - Tầng 3",
                style:
                    AppStyles.DEFAULT_LARGE_BOLD.copyWith(color: Colors.white)),
            flex: 2,
          ),
          Expanded(
            flex: 17,
            child: GridView.extent(
              crossAxisSpacing: MediaQuery.of(context).size.width / 20,
              maxCrossAxisExtent: MediaQuery.of(context).size.width / 3,
              children: listable,
            ),
          ),
        ],
      ),
    );
  }

  Widget buildAppointmentSchedule() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      padding: EdgeInsets.only(top: 8),
      decoration: BoxDecoration(
        color: Colors.grey[300],
        borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
      ),
      child: ListView.builder(
          itemCount: appointment.length,
          itemBuilder: (context, index) {
            return buildRowSchedule(customerBooking: appointment[index]);
          }),
    );
  }

  Widget buildRowSchedule({CustomerBooking customerBooking}) {
    var status = check(customerBooking.status);
    return ListTile(
      onTap: () => showBookingDetails(customerBooking),
      title: Container(
        decoration: BoxDecoration(
            color: customerBooking.status == TableStatus.CAME
                ? AppColors.DEFAULT_DANGER
                : Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(90),
                bottomLeft: Radius.circular(90),
                topRight: Radius.circular(45),
                bottomRight: Radius.circular(45)),
            // border: Border.all(width: 0.4, color: Colors.grey)),
            boxShadow: [
              BoxShadow(
                  color: AppColors.DEFAULT_DARK,
                  blurRadius: 2,
                  spreadRadius: 0.3,
                  offset: Offset(1, 0))
            ]),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: status["width"],
              height: status["height"],
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(90)),
                color: status["color"],
              ),
              child: Icon(Icons.book, color: Colors.white),
            ),
            Expanded(
              child: Container(
                alignment: Alignment(-0.2, 0),
                child: buildCustomerInfo(
                    customerBooking.customer, customerBooking.codeBooking),
              ),
            ),
            Container(
              height: status["height"],
              width: 50,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10),
                      bottomRight: Radius.circular(10)),
                  color: status["color"],
                  boxShadow: [
                    BoxShadow(
                        color: AppColors.DEFAULT_DARK,
                        blurRadius: 2,
                        spreadRadius: 0.5,
                        offset: Offset(1, 2))
                  ]),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.alarm,
                    color: Colors.white,
                  ),
                  Text(
                    "${customerBooking.timeBooking.DayNumber}:${customerBooking.timeBooking.DayInWeek}",
                    style:
                        AppStyles.DEFAULT_SMALL.copyWith(color: Colors.white),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildCustomerInfo(Customer customer, String codeBooking) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          "MSC: $codeBooking",
          style: AppStyles.DEFAULT_SMALL.copyWith(color: Colors.grey),
        ),
        SizedBox(
          height: 5,
        ),
        Text(customer.name, style: AppStyles.DEFAULT_MEDIUM_BOLD),
        Text(customer.phoneNumber, style: AppStyles.DEFAULT_MEDIUM_BOLD),
      ],
    );
  }

  showBookingDetails(CustomerBooking customerBooking) {
    return showDialog(
      context: context,
      builder: (context) => StatefulBuilder(
        builder: (context, setState) {
          return Material(
            color: AppColors.DEFAULT_DARK,
            elevation: 0,
            child: Stack(
              children: [
                Container(
                  padding: EdgeInsets.only(top: 80),
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    children: [
                      Expanded(
                        child: _builDialogDetails(customerBooking),
                        flex: 5,
                      ),
                      Expanded(
                        child: _builActionButtons(customerBooking),
                        flex: 8,
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment(-1, -0.95),
                  child: IconButton(
                    icon: Icon(Icons.close),
                    color: Colors.white,
                    onPressed: () => Navigator.of(context).pop(),
                    iconSize: 35,
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }

  _builDialogDetails(CustomerBooking booking) {
    var styleMedium =
        AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.white);
    var styleLarge = AppStyles.DEFAULT_LARGE_BOLD.copyWith(color: Colors.white);
    return Column(
      children: [
        Text(
          "MÃ ĐẶT CHỖ: ${booking.codeBooking}",
          style: styleLarge,
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "${booking.getTable()}",
                    style: styleMedium,
                  ),
                  Text(
                    "Bàn Số",
                    style: styleMedium,
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "${booking.area.DayInWeek}",
                    style: styleMedium,
                  ),
                  Text(
                    "Khu Vực",
                    style: styleMedium,
                  )
                ],
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    "${booking.level.DayInWeek}",
                    style: styleMedium,
                  ),
                  Text(
                    "Tầng",
                    style: styleMedium,
                  )
                ],
              ),
            ),
          ],
        ),
        Divider(
          color: Colors.white,
          thickness: 1,
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "${booking.customer.name}",
                    style: styleLarge,
                  ),
                  Text(
                    "Khách Hàng",
                    style: styleMedium,
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    "${booking.customer.phoneNumber}",
                    style: styleLarge,
                  ),
                  Text(
                    "Số Điện Thoại",
                    style: styleMedium,
                  ),
                ],
              ),
            )
          ],
        )
      ],
    );
  }

  _builActionButtons(CustomerBooking customerBooking) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          RaisedButton(
            color: AppColors.DEFAULT_PRIMARY,
            onPressed: () {
              setState(() {
                customerBooking.status = TableStatus.EMPTY;
                _saveSuccecs();
              });
            },
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(30))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Opacity(opacity: 0, child: Icon(Icons.sticky_note_2_outlined)),
                Text(
                  "KHÁCH ĐÃ ĐẾN",
                  style: AppStyles.DEFAULT_LARGE.copyWith(color: Colors.white),
                ),
                Icon(
                  Icons.sticky_note_2_outlined,
                  color: Colors.white,
                ),
              ],
            ),
          ),
          RaisedButton(
            color: AppColors.DEFAULT_SECONDARY,
            onPressed: () => null,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(30))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Opacity(opacity: 0, child: Icon(Icons.call)),
                Text(
                  "GỌI NGAY",
                  style: AppStyles.DEFAULT_LARGE.copyWith(color: Colors.white),
                ),
                Icon(Icons.call, color: Colors.white),
              ],
            ),
          ),
          RaisedButton(
            color: AppColors.DEFAULT_DANGER,
            onPressed: () {
              setState(() {
                customerBooking.status = TableStatus.CAME;
                _saveSuccecs();
              });
            },
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(30))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Opacity(opacity: 0, child: Icon(Icons.cancel_presentation)),
                Text(
                  "HỦY LỊCH HẸN",
                  style: AppStyles.DEFAULT_LARGE.copyWith(color: Colors.white),
                ),
                Icon(Icons.cancel_presentation, color: Colors.white),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _saveSuccecs() {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(
              "HÀNH ĐỘNG THÀNH CÔNG:",
              style: AppStyles.DEFAULT_MEDIUM_BOLD
                  .copyWith(color: AppColors.DEFAULT_DARK),
            ),
            content: Text(
              "Dữ Liệu Đã Được Cập Nhật!",
              style: AppStyles.DEFAULT_MEDIUM_BOLD
                  .copyWith(color: AppColors.DEFAULT_DARK),
            ),
          );
        });
  }

  check(status) {
    Color color;
    double height;
    double width;
    switch (status) {
      case TableStatus.EMPTY:
        color = AppColors.DEFAULT_PRIMARY;
        height = 100;
        width = 100;
        break;
      case TableStatus.BOOKED:
        color = AppColors.DEFAULT_WARRING;
        height = 60;
        width = 60;
        break;
      case TableStatus.CAME:
        color = AppColors.DEFAULT_DANGER;
        height = 60;
        width = 60;
        break;
      default:
        color = Colors.cyan;
        height = 60;
        width = 60;
        break;
    }
    return {"color": color, "height": height, "width": width};
  }
}
