import 'package:flutter/material.dart';
import 'package:app_foody_demo/src/configs/configs.dart';
import 'package:app_foody_demo/src/resource/resource.dart';
import 'package:provider/provider.dart';
import 'dart:math';

class BookingTable extends StatefulWidget {
  final ProviderCall providerNextPage;
  @override
  _BookingTableState createState() => _BookingTableState();
  BookingTable({this.providerNextPage});
}

class _BookingTableState extends State<BookingTable> {
  BookingViewModel provider;
  List<Day> listKhu;
  List<Day> listTang;
  bool isSwitched = false;
  bool switched = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    provider = Provider.of<BookingViewModel>(context,listen: false);
    listKhu = AppSeedingData.LIST_AREA;
    listTang = AppSeedingData.LIST_LEVEL;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 8),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: [
          buildCheckinInfo(),
          buildBookingTableLocation(),
          buildBookingTable()
        ],
      ),
    );
  }

  Widget buildCheckinInfo() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: [
        Expanded(
          child: Column(
            children: [
              Text(
                provider.chossenDay == null ? "0" : "${provider.chossenDay.DayNumber}/${provider.chossenDay.DayInWeek}",
                style: AppStyles.DEFAULT_MEDIUM_BOLD,
              ),
              Text(
                "Ngày Hẹn",
                style: AppStyles.DEFAULT_SMALL,
              )
            ],
          ),
        ),
        Expanded(
          child: Center(
            child: Column(
              children: [
                Text(
                  provider.chossenHour == null ? "0" : "${provider.chossenHour.DayNumber}:${provider.chossenHour.DayInWeek}",
                  style: AppStyles.DEFAULT_MEDIUM_BOLD,
                ),
                Text(
                  "Giờ hẹn",
                  style: AppStyles.DEFAULT_SMALL,
                )
              ],
            ),
          ),
        ),
        Expanded(
          child: Column(
            children: [
              Text(
                provider.capacity == null ? "0" : "${provider.capacity}",
                style: AppStyles.DEFAULT_MEDIUM_BOLD,
              ),
              Text(
                "Chỗ",
                style: AppStyles.DEFAULT_SMALL,
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget buildBookingTableLocation() {
    return Container(
      height: 160,
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "CHỌN BÀN",
            style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(letterSpacing: 2),
          ),
          buildAreaChose(),
          buildLevelChose(),
          buildFooterBookingLocation(),
        ],
      ),
    );
  }

  Widget buildAreaChose() {
    return Container(
      width: 310,
      padding: EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(30)),
        boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 10)],
        // border:Border.all(width: 1, color: )
      ),
      height: 40,
      child: DropdownButton(
        underline: Container(
          color: Colors.white,
        ),
        isExpanded: true,
        elevation: 16,
        icon: Icon(Icons.keyboard_arrow_down),
        value: Provider.of<BookingViewModel>(context,listen: false).area,
        items: buildDropItem(),
        onChanged: (value) {
          setState(() {
            Provider.of<BookingViewModel>(context,listen: false).area = value;
          });
        },
      ),
    );
  }

  List<DropdownMenuItem<String>> buildDropItem() {
    List<DropdownMenuItem<String>> listWg = [];
    for (int i = 0; i < listKhu.length; i++) {
      listWg.add(DropdownMenuItem(
        child: Text(
          listKhu[i].DayNumber.toString(),
          style:
              AppStyles.DEFAULT_MEDIUM.copyWith(color: AppColors.DEFAULT_DARK),
        ),
        value: listKhu[i].DayInWeek,
      ));
    }
    return listWg.toList();
  }

  Widget buildLevelChose() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      width: 310,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(30)),
        boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 10)],
        // border:Border.all(width: 1, color: )
      ),
      height: 40,
      child: DropdownButton(
        underline: Container(
          color: Colors.white,
        ),
        isExpanded: true,
        elevation: 16,
        icon: Icon(Icons.keyboard_arrow_down),
        value: Provider.of<BookingViewModel>(context,listen: false).level,
        items: buildDropItemLevel(),
        onChanged: (value) {
          setState(() {
            Provider.of<BookingViewModel>(context,listen: false).level = value;
          });
        },
      ),
    );
  }

  List<DropdownMenuItem<String>> buildDropItemLevel() {
    List<DropdownMenuItem<String>> listWg = [];
    for (int i = 0; i < listTang.length; i++) {
      listWg.add(DropdownMenuItem(
        child: Text(
          listTang[i].DayNumber.toString(),
          style:
              AppStyles.DEFAULT_MEDIUM.copyWith(color: AppColors.DEFAULT_DARK),
        ),
        value: listTang[i].DayInWeek,
      ));
    }
    return listWg.toList();
  }

  Widget buildFooterBookingLocation() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Container(
                width: 16,
                height: 16,
                child: CircleAvatar(
                  backgroundColor: AppColors.DEFAULT_PRIMARY,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "Còn Trống",
                style: AppStyles.DEFAULT_SMALL,
              )
            ],
          ),
          Row(
            children: [
              Container(
                width: 16,
                height: 16,
                child: CircleAvatar(
                  backgroundColor: AppColors.DEFAULT_WARRING,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "Đã Đặt",
                style: AppStyles.DEFAULT_SMALL,
              )
            ],
          ),
          Row(
            children: [
              Container(
                width: 16,
                height: 16,
                child: CircleAvatar(
                  backgroundColor: AppColors.DEFAULT_DANGER,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "Khách Đã Tới",
                style: AppStyles.DEFAULT_SMALL,
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget buildBookingTable() {
    return Expanded(
      child: Container(
        padding: EdgeInsets.only(left: 16, right: 16, top: 8),
        color: AppColors.DEFAULT_DARK,
        child: buildBookingTableFooterBody(),
      ),
    );
  }

  Widget buildBookingTableFooterBody() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              "${listKhu[listKhu.indexWhere((element) => element.DayInWeek == Provider.of<BookingViewModel>(context,listen: false).area)].DayNumber.toUpperCase()}-",
              style: AppStyles.DEFAULT_LARGE
                  .copyWith(color: Colors.white, letterSpacing: 2),
            ),
            Text(
                "${listTang[listTang.indexWhere((element) => element.DayInWeek == Provider.of<BookingViewModel>(context,listen: false).level)].DayNumber.toUpperCase()}",
                style: AppStyles.DEFAULT_MEDIUM
                    .copyWith(color: Colors.white, letterSpacing: 2))
          ],
        ),
        Expanded(
          flex: 14,
          child: Container(
            child: buildWrapView(),
          ),
        ),
        Expanded(
          flex: 3,
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                Text(
                  "Chọn Cả Khu",
                  style: AppStyles.DEFAULT_SMALL.copyWith(color: Colors.white),
                ),
                Switch(
                  value: isSwitched,
                  onChanged: (value) {
                    setState(() {
                      isSwitched = value;
                    });
                  },
                  activeTrackColor: AppColors.DEFAULT_INFO,
                  activeColor: Colors.white,
                  inactiveTrackColor: AppColors.DEFAULT_LIGHT,
                )
              ],
            ),
          ),
        ),
        Expanded(
          flex: 3,
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                Text("Chọn Cả Tầng",
                    style:
                        AppStyles.DEFAULT_SMALL.copyWith(color: Colors.white)),
                Switch(
                  value: isSwitched,
                  onChanged: (value) {
                    setState(() {
                      isSwitched = value;
                    });
                  },
                  activeTrackColor: AppColors.DEFAULT_INFO,
                  activeColor: Colors.white,
                  inactiveTrackColor: AppColors.DEFAULT_LIGHT,
                )
              ],
            ),
          ),
        ),
        SizedBox(
          width: 275,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(30))),
            color: AppColors.DEFAULT_PRIMARY,
            onPressed: () => ShowModal(context),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Opacity(
                    opacity: 0, child: Image.asset(AppImages.ICON_ARROW_X1)),
                Text(
                  "NHẬP THÔNG TIN",
                  textAlign: TextAlign.center,
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.white),
                ),
                Image.asset(AppImages.ICON_ARROW_X1)
              ],
            ),
          ),
        ),
      ],
    );
  }

  ShowModal(BuildContext context) {
    Customer customer = new Customer();
    final nameController = TextEditingController();
    final phoneNumberController = TextEditingController();
    final customerCapacity = TextEditingController();
    final birthDayController = TextEditingController();
    return showDialog(
      context: context,
      builder: (context) => StatefulBuilder(
        builder: (context, setState) {
          return Material(
            type: MaterialType.transparency,
            child: Stack(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 100, horizontal: 30),
                  // A simplified version of dialog.
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  color: Colors.transparent,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          'THÔNG TIN KHÁCH HÀNG',
                          style: AppStyles.DEFAULT_LARGE_BOLD
                              .copyWith(color: Colors.white),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        CustomSizeBox(
                          nameController,
                          "Họ Và Tên",
                          true,
                          height: 40,
                          change: (value) {
                            customer.id = 1;
                            customer.name = value;
                          },
                        ),
                        GestureDetector(
                          onTap: () {
                            showDatePicker(
                                    context: context,
                                    initialDate: DateTime.now(),
                                    firstDate: DateTime(1960),
                                    lastDate: DateTime(2030))
                                .then((value) => {
                                      setState(() {
                                        customer.birth = value;
                                        birthDayController.text = "${value.toLocal()}".split(' ')[0];
                                      })
                                    });
                          },
                          child: CustomSizeBox(
                            birthDayController,
                            "Sinh Nhật",
                            false,
                            height: 40,
                            surfix: Icon(
                              Icons.calendar_today_sharp,
                              size: 20,
                            ),
                          ),
                        ),
                        CustomSizeBox(
                          phoneNumberController,
                          "Số Điện Thoại",
                          true,
                          height: 40,
                          change: (value) {
                            customer.phoneNumber = value;
                          },
                        ),
                        CustomSizeBox(
                          customerCapacity,
                          "Lượng Khách Sẽ Đến",
                          true,
                          height: 40,
                          change: (value) {
                            Provider.of<BookingViewModel>(context,listen: false)
                                .capacity = int.parse(value);
                          },
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            CustomSizeBox(
                              null,
                              "Số Lượng Nam",
                              true,
                              height: 40,
                              width: 145,
                            ),
                            CustomSizeBox(
                              null,
                              "Số Lượng Nữ",
                              true,
                              height: 40,
                              width: 145,
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Gửi SMS Thông Báo",
                              style: AppStyles.DEFAULT_MEDIUM
                                  .copyWith(color: Colors.white),
                            ),
                            Switch(
                              value: switched,
                              onChanged: (value) {
                                setState(() {
                                  switched = value;
                                });
                              },
                              activeTrackColor: AppColors.DEFAULT_INFO,
                              activeColor: Colors.white,
                              inactiveTrackColor: AppColors.DEFAULT_LIGHT,
                            )
                          ],
                        ),
                        RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30))),
                          color: AppColors.DEFAULT_PRIMARY,
                          onPressed: () {
                            Provider.of<BookingViewModel>(context,listen: false).customer = customer;
                            Provider.of<BookingViewModel>(context,listen: false).isMessage = switched;
                            Navigator.of(context).pop();
                            widget.providerNextPage();
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Opacity(
                                  opacity: 0,
                                  child: Image.asset(AppImages.ICON_ARROW_X1)),
                              Text(
                                "XONG",
                                textAlign: TextAlign.center,
                                style: AppStyles.DEFAULT_LARGE
                                    .copyWith(color: Colors.white),
                              ),
                              Image.asset(AppImages.ICON_ARROW_X1)
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment(-1, -0.95),
                  child: IconButton(
                    icon: Icon(Icons.close),
                    color: Colors.white,
                    onPressed: () => Navigator.of(context).pop(),
                    iconSize: 35,
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }

  Widget buildWrapView() {
    return GridView.extent(
      crossAxisSpacing: MediaQuery.of(context).size.width / 20,
      maxCrossAxisExtent: MediaQuery.of(context).size.width / 3,
      children: AppSeedingData.LIST_TABLE,
    );
  }
}

enum TableStatus {
  EMPTY,
  BOOKED,
  CAME,
}

class TableDraw extends StatefulWidget {
  final int id;
  final String name;
  final int chairsSlot;
  final TableStatus status;
  final int booked;
  TableDraw(this.id, this.chairsSlot, this.status, this.name, this.booked);
  @override
  _TableDrawState createState() => _TableDrawState();
}

class _TableDrawState extends State<TableDraw> {
  @override
  bool saved;
  List<Align> drawTable() {
    final list = <Align>[];
    list.add(Align(
      alignment: Alignment(0, 0),
      child: Container(
        height: 25,
        width: 25,
        decoration: BoxDecoration(
          color: Chair.check(widget.status),
          shape: BoxShape.circle,
        ),
        child: Center(
          child: Text(
            '${widget.booked}/${widget.chairsSlot}',
            style: TextStyle(
              fontSize: 10,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    ));
    for (int i = 0; i < widget.chairsSlot; i++) {
      var degree = 360 / widget.chairsSlot * (i);
      var radian = degree * (pi / 180);
      list.add(Align(
        alignment:
            Alignment(sin(radian - 3 * pi / 4), cos(radian - 3 * pi / 4)),
        child: Container(
            height: 28,
            padding: EdgeInsets.all(8),
            child: Transform.rotate(
              angle: 3 * pi / 4 - radian,
              child: i < widget.booked
                  ? Chair(TableStatus.BOOKED)
                  : Chair(widget.status),
            )),
      ));
    }
    list.add(Align(
      alignment: Alignment(1, -1),
      child: saved ? Image.asset(AppImages.CHECK_PRIMARY_X1) : null,
    ));
    return list;
  }

  @override
  Widget build(BuildContext context) {
    saved = Provider.of<BookingViewModel>(context,listen: false)
        .ListTable
        .contains(this.widget);
    return GestureDetector(
      onTap: () {
        setState(() {
          if (saved) {
            Provider.of<BookingViewModel>(context,listen: false)
                .ListTable
                .remove(this.widget);
            saved = false;
          } else {
            Provider.of<BookingViewModel>(context,listen: false)
                .ListTable
                .add(this.widget);
            saved = true;
          }
        });
      },
      child: Column(
        children: [
          Container(
            width: 70,
            decoration: saved
                ? BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10)))
                : null,
            height: 70,
            child: Stack(
              children: drawTable(),
            ),
          ),
          Text(
            "${widget.name}",
            style: AppStyles.DEFAULT_SMALL.copyWith(color: Colors.white),
          ),
        ],
      ),
    );
  }
}

class Chair extends StatelessWidget {
  TableStatus status;
  Chair(this.status);

  static check(status) {
    Color color;
    switch (status) {
      case TableStatus.EMPTY:
        color = AppColors.DEFAULT_PRIMARY;
        break;
      case TableStatus.BOOKED:
        color = AppColors.DEFAULT_WARRING;
        break;
      case TableStatus.CAME:
        color = AppColors.DEFAULT_DANGER;
        break;
      default:
        color = Colors.cyan;
        break;
    }
    return color;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Image.asset(
      AppImages.CHAIR,
      color: check(status),
      fit: BoxFit.fill,
    );
  }
}
