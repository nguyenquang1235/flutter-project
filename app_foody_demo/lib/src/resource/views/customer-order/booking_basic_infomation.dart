import 'package:flutter/material.dart';
import 'package:app_foody_demo/src/resource/resource.dart';
import 'customer_order_view.dart';
import 'package:provider/provider.dart';

class BookingBasicInfomation extends StatefulWidget {
  final ProviderCall providerNextPage;
  @override
  _BookingBasicInfomationState createState() => _BookingBasicInfomationState();
  BookingBasicInfomation({this.providerNextPage});
}

class _BookingBasicInfomationState extends State<BookingBasicInfomation>
    with TickerProviderStateMixin {
  TabController DayController;
  TabController TimeController;
  int _dayChossen;
  int _timeChossen;
  int _capacity;
  String _valueKhu;
  String _valueTang;
  List<Day> ListDay;
  List<Day> ListHours;
  List<Day> ListKhu;
  List<Day> ListTang;
  @override
  void initState() {
    super.initState();
    _valueKhu = "1";
    _valueTang = "1";
    ListDay = AppSeedingData.LIST_DAY;
    ListHours = AppSeedingData.LIST_TIME;
    ListKhu = AppSeedingData.LIST_AREA;
    ListTang = AppSeedingData.LIST_LEVEL;
    DayController = new TabController(length: ListDay.length, vsync: this);
    TimeController = new TabController(length: ListHours.length, vsync: this);
  }

  @override
  void dispose() {
    DayController.dispose();
    TimeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Expanded(child: buildChooseDay(), flex: 2,),
          Expanded(child: buildChooseTime(), flex: 2,),
          Expanded(child: buildChosseLocation(), flex: 4,),
        ],
      ),
    );
  }

  //Build ChooseDay

  Widget buildChooseDay() {
    return Container(
      margin: EdgeInsets.only(top: 10),
      height: 130,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            "CHỌN NGÀY",
            style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(letterSpacing: 2),
          ),
          TabBar(
            controller: DayController,
            isScrollable: true,
            indicator: BoxDecoration(),
            labelPadding: EdgeInsets.symmetric(horizontal: 4),
            tabs: builDayRow(),
          )
        ],
      ),
    );
  }

  List<Widget> builDayRow() {
    List<Widget> list = [];
    for (var i = 0; i < ListDay.length; i++) {
      list.add(GestureDetector(
        onTap: () {
          setState(() {
            _dayChossen = i;
          });
        },
        child: Container(
          height: _dayChossen == i ? 90 : 80,
          width: _dayChossen == i ? 60 : 50,
          padding: EdgeInsets.symmetric(vertical: 10),
          decoration: BoxDecoration(
              color: _dayChossen == i ? AppColors.DEFAULT_INFO : Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              border: _dayChossen == i
                  ? null
                  : Border.all(width: 0.5, color: AppColors.DEFAULT_DARK)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("${ListDay[i].DayNumber}",
                  style: _dayChossen == i
                      ? AppStyles.DEFAULT_VERY_LARGE_BOLD
                          .copyWith(color: Colors.white)
                      : AppStyles.DEFAULT_LARGE_BOLD
                          .copyWith(color: AppColors.DEFAULT_DARK)),
              Text("${ListDay[i].DayInWeek}",
                  style: _dayChossen == i
                      ? AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.white)
                      : AppStyles.DEFAULT_SMALL
                          .copyWith(color: AppColors.DEFAULT_DARK)),
            ],
          ),
        ),
      ));
    }
    return list;
  }

  //Build ChooseTime

  Widget buildChooseTime() {
    return Container(
        height: 75,
        margin: EdgeInsets.only(bottom: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              "CHỌN GIỜ",
              style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(letterSpacing: 2),
            ),
            TabBar(
              controller: TimeController,
              isScrollable: true,
              indicator: BoxDecoration(),
              labelPadding: EdgeInsets.symmetric(horizontal: 4),
              tabs: buildHourRow(),
            )
          ],
        ));
  }

  List<Widget> buildHourRow() {
    List<Widget> list = [];
    for (int i = 0; i < ListHours.length; i++) {
      list.add(GestureDetector(
        onTap: () {
          setState(() {
            _timeChossen = i;
          });
        },
        child: Container(
          alignment: Alignment(0, 0),
          width: 80,
          height: 30,
          child: Text(
            "${ListHours[i].DayNumber}:${ListHours[i].DayInWeek}",
            style: AppStyles.DEFAULT_MEDIUM.copyWith(
                color:
                    _timeChossen == i ? Colors.white : AppColors.DEFAULT_DARK),
          ),
          decoration: BoxDecoration(
              color:
                  _timeChossen == i ? AppColors.DEFAULT_WARRING : Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(20)),
              border: _timeChossen == i
                  ? null
                  : Border.all(color: AppColors.DEFAULT_DARK, width: 0.5)),
        ),
      ));
    }
    return list;
  }

  //Build ChosseLocation

  Widget buildChosseLocation() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      width: 360,
      height: 280,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.COMPONENT_X1), fit: BoxFit.fill)),
      child: Column(
        children: [
          Text(
            "CHỌN VỊ TRÍ",
            style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(letterSpacing: 2),
          ),
          SizedBox(
            height: 20,
          ),
          buildCustomerChoselocationBody(),
        ],
      ),
    );
  }

  Widget buildCustomerChoselocationBody() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 10)]),
              height: 40,
              child: TextFormField(
                onChanged: (text) {
                  _capacity = int.parse(text);
                },
                keyboardType: TextInputType.number,
                textAlignVertical: TextAlignVertical.bottom,
                decoration: InputDecoration(
                    hintStyle: AppStyles.DEFAULT_MEDIUM
                        .copyWith(color: AppColors.DEFAULT_DARK),
                    filled: true,
                    fillColor: Colors.white,
                    hintText: "Số Lượng Khách",
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        borderSide: BorderSide(color: Colors.white)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        borderSide: BorderSide(color: Colors.white))),
              )),
          SizedBox(
            height: 10,
          ),
          buildAreaChose(),
          SizedBox(
            height: 10,
          ),
          buildLevelChose(),
          SizedBox(
            height: 30,
          ),
          buildButonSubmit()
        ],
      ),
    );
  }

  Widget buildAreaChose() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(30)),
        boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 10)],
        // border:Border.all(width: 1, color: )
      ),
      height: 40,
      child: DropdownButton(
        underline: Container(
          color: Colors.white,
        ),
        isExpanded: true,
        elevation: 16,
        icon: Icon(Icons.keyboard_arrow_down),
        value: _valueKhu,
        items: buildDropItem(),
        onChanged: (value) {
          setState(() {
            _valueKhu = value;
          });
        },
      ),
    );
  }

  List<DropdownMenuItem<String>> buildDropItem() {
    List<DropdownMenuItem<String>> listWg = [];
    for (int i = 0; i < ListKhu.length; i++) {
      listWg.add(DropdownMenuItem(
        child: Text(
          ListKhu[i].DayNumber.toString(),
          style:
              AppStyles.DEFAULT_MEDIUM.copyWith(color: AppColors.DEFAULT_DARK),
        ),
        value: ListKhu[i].DayInWeek,
      ));
    }
    return listWg.toList();
  }

  Widget buildLevelChose() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(30)),
        boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 10)],
        // border:Border.all(width: 1, color: )
      ),
      height: 40,
      child: DropdownButton(
        underline: Container(
          color: Colors.white,
        ),
        isExpanded: true,
        elevation: 16,
        icon: Icon(Icons.keyboard_arrow_down),
        value: _valueTang,
        items: buildDropItemLevel(),
        onChanged: (value) {
          setState(() {
            _valueTang = value;
          });
        },
      ),
    );
  }

  List<DropdownMenuItem<String>> buildDropItemLevel() {
    List<DropdownMenuItem<String>> listWg = [];
    for (int i = 0; i < ListTang.length; i++) {
      listWg.add(DropdownMenuItem(
        child: Text(
          ListTang[i].DayNumber.toString(),
          style:
              AppStyles.DEFAULT_MEDIUM.copyWith(color: AppColors.DEFAULT_DARK),
        ),
        value: ListTang[i].DayInWeek,
      ));
    }
    return listWg.toList();
  }

  Widget buildButonSubmit() {
    return RaisedButton(
      color: AppColors.DEFAULT_PRIMARY,
      onPressed: () {
        try {
          Provider.of<BookingViewModel>(context, listen: false).chossenDay =
              ListDay[_dayChossen];
          Provider.of<BookingViewModel>(context, listen: false).chossenHour =
              ListDay[_timeChossen];
          Provider.of<BookingViewModel>(context, listen: false).capacity =
              _capacity;
          Provider.of<BookingViewModel>(context, listen: false).area =
              _valueKhu;
          Provider.of<BookingViewModel>(context, listen: false).level =
              _valueTang;
        } catch (e) {
          print('Lỗi xảy ra' + e.message);
        } finally {
          if (_dayChossen != null &&
              _capacity != null &&
              _timeChossen != null) {
            widget.providerNextPage();
          } else {
            showDialog(
                context: context,
                builder: (context) => AlertDialog(
                      title: Text("THIẾU THÔNG TIN:"),
                      content: Text("Bạn chưa điền đẩy đủ thông tin!"),
                    ));
          }
        }
      },
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(30))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Opacity(opacity: 0, child: Image.asset(AppImages.ICON_ARROW_X1)),
          Text(
            "TIẾP THEO",
            style: AppStyles.DEFAULT_LARGE.copyWith(color: Colors.white),
          ),
          Image.asset(AppImages.ICON_ARROW_X1),
        ],
      ),
    );
  }
}

typedef ProviderCall = void Function();
typedef ParamFunc = void Function(String value);
