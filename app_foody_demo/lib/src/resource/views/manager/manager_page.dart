import 'package:flutter/material.dart';

import '../../resource.dart';

class ManagerPage extends StatefulWidget {
  @override
  _ManagerPageState createState() => _ManagerPageState();
}

class _ManagerPageState extends State<ManagerPage>
    with SingleTickerProviderStateMixin {
  TabController tabcontroller;

  @override
  void initState() {
    super.initState();
    tabcontroller = TabController(length: 4, vsync: this);
  }

  @override
  void dispose() {
    tabcontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: Stack(
          children: [
            Column(
              children: [
                Expanded(
                  child: _buildTabMenu(),
                  flex: 1,
                ),
                Expanded(
                  child: _buildTabBar(),
                  flex: 2,
                ),
                Expanded(
                  child: _buildTabViewBody(),
                  flex: 12,
                ),
              ],
            ),
            Align(
              child: Image.asset(AppImages.CHEF_X1),
              alignment: Alignment(0, -0.95),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTabMenu() {
    return Container(
      height: 70,
      padding: EdgeInsets.only(bottom: 20, left: 10, right: 10),
      color: AppColors.DEFAULT_PRIMARY,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.asset(AppImages.ICON_BACK_ARROW_X1),
          Image.asset(AppImages.ICON_MENU_X1)
        ],
      ),
    );
  }

  Widget _buildTabBar() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 120,
      decoration: BoxDecoration(
          color: AppColors.DEFAULT_DARK,
          borderRadius: BorderRadius.vertical(bottom: Radius.circular(10))),
      child: TabBar(
        controller: tabcontroller,
        labelPadding: EdgeInsets.only(left: 15, right: 15, top: 16),
        isScrollable: true,
        indicator: UnderlineTabIndicator(
            insets: EdgeInsets.only(bottom: 20, left: 30, right: 30),
            borderSide: BorderSide(color: AppColors.DEFAULT_WARRING, width: 5)),
        labelColor: AppColors.DEFAULT_WARRING,
        labelStyle: AppStyles.DEFAULT_MEDIUM_BOLD,
        unselectedLabelColor: Colors.white,
        unselectedLabelStyle: AppStyles.DEFAULT_SMALL_BOLD,
        tabs: [
          Tab(
            text: "KHÁCH HÀNG",
          ),
          Tab(
            text: "MÓN ĂN",
          ),
          Tab(
            text: "DOANH THU",
          ),
          Tab(
            text: "CƠ SỞ",
          ),
        ],
      ),
    );
  }

  Widget _buildTabViewBody() {
    return TabBarView(
      controller: tabcontroller,
      children: [
        CustomerManagerPage(),
        Center(child: Text("Món ăn")),
        Center(child: Text("Doanh Thu")),
        Center(child: Text("Cở sở")),
      ],
    );
  }
}
