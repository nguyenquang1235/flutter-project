import 'package:app_foody_demo/src/resource/models/restaurant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:math';
import 'package:provider/provider.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:app_foody_demo/src/resource/resource.dart';

class CustomerManagerPage extends StatefulWidget {
  @override
  _CustomerManagerPageState createState() => _CustomerManagerPageState();
}

class _CustomerManagerPageState extends State<CustomerManagerPage>
    with TickerProviderStateMixin {
  TabController tabcontroller;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    tabcontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Expanded(
            child: _buildHeaderPage(),
            flex: 3,
          ),
          Expanded(
            child: _buildBodyPage(),
            flex: 12,
          )
        ],
      ),
    );
  }

  Widget _buildHeaderPage() {
    return Consumer<CustomerManagerProvider>(
      builder: (context, value, child) {
        tabcontroller = TabController(
          length: 6,
          vsync: this,
          initialIndex: AppSeedingData.LIST_DAY
              .indexOf(Provider.of<CustomerManagerProvider>(context).day),
        );
        return Container(
          padding: const EdgeInsets.all(16),
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 2,
                child: Text(
                  "10:00 20/05/2020",
                  style: AppStyles.DEFAULT_SMALL,
                ),
              ),
              Expanded(
                flex: 4,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Thống Kê Số Lượng Khách Hàng",
                        style: AppStyles.DEFAULT_MEDIUM_BOLD
                            .copyWith(letterSpacing: 1.5)),
                    CircleAvatar(
                      backgroundColor: Colors.black,
                      child: Transform.rotate(
                        angle: pi / 2,
                        child: const Icon(
                          Icons.add_road_sharp,
                          color: Colors.white,
                          size: 15,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                flex: 7,
                child: TabBar(
                  tabs: _buildDayTab(),
                  isScrollable: true,
                  controller: tabcontroller,
                  labelPadding:
                      const EdgeInsets.only(left: 15, right: 15, top: 15),
                  labelColor: AppColors.DEFAULT_WARRING,
                  unselectedLabelColor: Colors.grey.withOpacity(0.8),
                  labelStyle: AppStyles.DEFAULT_MEDIUM_BOLD,
                  onTap: (value) {
                    Provider.of<CustomerManagerProvider>(context, listen: false)
                        .changeDay(AppSeedingData.LIST_DAY[value]);
                  },
                  indicator: UnderlineTabIndicator(
                      insets: const EdgeInsets.only(left: 30, right: 30),
                      borderSide: const BorderSide(
                          color: AppColors.DEFAULT_WARRING, width: 2)),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  List<Tab> _buildDayTab() {
    List<Tab> listTab = new List<Tab>();
    for (var item in AppSeedingData.LIST_DAY) {
      listTab.add(
        Tab(
          text: item.DayNumber == "18" ? "HÔM NAY" : item.DayNumber,
        ),
      );
    }

    return listTab;
  }

  Widget _buildBodyPage() {
    return Container(
      color: AppColors.DEFAULT_DARK,
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 16),
      child: Column(
        children: [
          _buildDropMenu(),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  _buildDetailData(),
                  _buildButtonOutput(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildButtonOutput() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: RaisedButton(
        shape: const RoundedRectangleBorder(
            borderRadius: const BorderRadius.all(Radius.circular(30))),
        color: AppColors.DEFAULT_PRIMARY,
        onPressed: () => null,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "XUẤT BÁO CÁO",
              textAlign: TextAlign.center,
              style: AppStyles.DEFAULT_LARGE.copyWith(color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }

  Future<List<DropdownMenuItem<int>>> _buildRestaurantDropList() async {
    List<DropdownMenuItem<int>> listWg = [];
    for (var item in AppSeedingData.LIST_RESTAURANT) {
      listWg.add(
        DropdownMenuItem(
          child: Text(
            item.name,
            style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.black),
          ),
          value: item.id,
        ),
      );
    }
    return listWg;
  }

  Widget _buildDropMenu() {
    return FutureBuilder<List<DropdownMenuItem<int>>>(
      future: _buildRestaurantDropList(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: LoadingBouncingLine.circle(
              size: 100,
              backgroundColor: Colors.white,
            ),
          );
        }
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          margin: const EdgeInsets.only(bottom: 8),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: const BorderRadius.all(Radius.circular(30))),
          child: Consumer<CustomerManagerProvider>(
            builder: (context, value, child) => DropdownButton(
              elevation: 16,
              underline: Container(
                color: Colors.white,
              ),
              isExpanded: true,
              icon: const Icon(Icons.keyboard_arrow_down),
              value: Provider.of<CustomerManagerProvider>(context, listen: true)
                  .idRestaurant,
              items: snapshot.data,
              onChanged: (value) {
                Provider.of<CustomerManagerProvider>(context, listen: false)
                    .changeIdRestaurant(value);
              },
            ),
          ),
        );
      },
    );
  }

  Widget _buildDetailData() {
    return Consumer<CustomerManagerProvider>(builder: (context, value, child) {
      Provider.of<CustomerManagerProvider>(context, listen: false)
          .queryRestaurantData();
      var data =
          Provider.of<CustomerManagerProvider>(context, listen: true).data;
      var restaurant = AppSeedingData.LIST_RESTAURANT
          .where((element) => data.restaurantId == element.id)
          .first;
      return Column(
        children: [
          _buildOverViewData(data),
          _buildWrapChart(data, restaurant),
          _buildListCustomer(restaurant.listCustomer),
        ],
      );
    });
  }

  Widget _buildOverViewData(RestaurantData data) {
    var large = AppStyles.DEFAULT_LARGE_BOLD.copyWith(color: Colors.white);
    var medium = AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.white);
    const iconUp = Icon(
      Icons.arrow_upward,
      color: AppColors.DEFAULT_INFO,
      size: 20,
    );
    const iconDown = Icon(
      Icons.arrow_downward,
      color: AppColors.DEFAULT_DANGER,
      size: 20,
    );
    const baseline = CrossAxisAlignment.baseline;
    const start = CrossAxisAlignment.start;
    const end = CrossAxisAlignment.end;

    return AspectRatio(
      aspectRatio: 9 / 5,
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              crossAxisAlignment: end,
              children: [
                Expanded(
                  flex: 8,
                  child: Row(
                    crossAxisAlignment: baseline,
                    children: [
                      Icon(
                        Icons.person,
                        color: Colors.white,
                        size: 22,
                      ),
                      Column(
                        crossAxisAlignment: start,
                        children: [
                          Text(
                            data.booking.toString(),
                            style: AppStyles.DEFAULT_VERY_LARGE_BOLD
                                .copyWith(color: Colors.white),
                          ),
                          Text(
                            "Khách Đặt Bàn",
                            style: medium,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Row(
                    crossAxisAlignment: baseline,
                    children: [
                      iconUp,
                      Column(
                        crossAxisAlignment: start,
                        children: [
                          Text(
                            "5%",
                            style: large,
                          ),
                          Text(
                            "Hôm Qua",
                            style: medium,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 7,
                  child: Row(
                    crossAxisAlignment: baseline,
                    children: [
                      iconDown,
                      Column(
                        crossAxisAlignment: start,
                        children: [
                          Text(
                            "2%",
                            style: large,
                          ),
                          Text(
                            "1 Tháng Trước",
                            style: medium,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: end,
              children: [
                Expanded(
                  flex: 8,
                  child: Row(
                    crossAxisAlignment: baseline,
                    children: [
                      Opacity(
                        child: Icon(
                          Icons.person,
                          color: Colors.white,
                          size: 22,
                        ),
                        opacity: 0,
                      ),
                      Column(
                        crossAxisAlignment: start,
                        children: [
                          Row(
                            crossAxisAlignment: end,
                            children: [
                              Text((data.booking - data.cancel).toString(),
                                  style: large),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                  "(${((data.booking - data.cancel) / data.booking * 100).toStringAsFixed(0)}%)",
                                  style: medium),
                            ],
                          ),
                          Text(
                            "Tỷ Lệ Khách Đến",
                            style: medium,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Row(
                    crossAxisAlignment: baseline,
                    children: [
                      iconUp,
                      Column(
                        crossAxisAlignment: start,
                        children: [
                          Text(
                            "5%",
                            style: large,
                          ),
                          Text(
                            "Hôm Qua",
                            style: medium,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 7,
                  child: Row(
                    crossAxisAlignment: baseline,
                    children: [
                      iconDown,
                      Column(
                        crossAxisAlignment: start,
                        children: [
                          Text(
                            "2%",
                            style: large,
                          ),
                          Text(
                            "1 Tháng Trước",
                            style: medium,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: end,
              children: [
                Expanded(
                  flex: 8,
                  child: Row(
                    crossAxisAlignment: baseline,
                    children: [
                      Opacity(
                        child: Icon(
                          Icons.person,
                          color: Colors.white,
                          size: 22,
                        ),
                        opacity: 0,
                      ),
                      Column(
                        crossAxisAlignment: start,
                        children: [
                          Row(
                            crossAxisAlignment: end,
                            children: [
                              Text(data.cancel.toString(), style: large),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                  "(${(data.cancel / data.booking * 100).toStringAsFixed(0)}%)",
                                  style: medium),
                            ],
                          ),
                          Text(
                            "Tỷ Lệ Khách Hủy",
                            style: medium,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Row(
                    crossAxisAlignment: baseline,
                    children: [
                      iconUp,
                      Column(
                        crossAxisAlignment: start,
                        children: [
                          Text(
                            "5%",
                            style: large,
                          ),
                          Text(
                            "Hôm Qua",
                            style: medium,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 7,
                  child: Row(
                    crossAxisAlignment: baseline,
                    children: [
                      iconDown,
                      Column(
                        crossAxisAlignment: start,
                        children: [
                          Text(
                            "2%",
                            style: large,
                          ),
                          Text(
                            "1 Tháng Trước",
                            style: medium,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildWrapChart(RestaurantData data, Restaurant restaurant) {
    return Wrap(
      direction: Axis.horizontal,
      runAlignment: WrapAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Expanded(
              child: AppCustomChart(
                title: "Số Lượng Đặt Bàn",
                colorThisWeek: AppColors.DEFAULT_INFO,
                radito: 1 / 1.15,
                subTitle: 20 + Random().nextInt(50 - 20),
              ),
            ),
            Expanded(
              child: AppCustomChart(
                title: "Tỷ Lệ Khách Lấp Đầy Bàn",
                colorThisWeek: AppColors.DEFAULT_WARRING,
                radito: 1 / 1.15,
              ),
            ),
          ],
        ),
        _buildChartWithInfo(
          title: "Khách Hàng Mới Tuần Này",
          sub: restaurant.newCustomer,
          color: AppColors.DEFAULT_SECONDARY,
          icon: Icon(
            Icons.arrow_upward,
            color: AppColors.DEFAULT_INFO,
            size: 20,
          ),
          percent: restaurant.newCustomer / (data.booking - data.cancel) * 100,
        ),
        _buildChartWithInfo(
          title: "Khách Lần Đầu Trở Lại",
          sub: restaurant.newCustomerReturn,
          color: AppColors.DEFAULT_DANGER,
          icon: Icon(
            Icons.arrow_upward,
            color: AppColors.DEFAULT_INFO,
            size: 20,
          ),
          percent:
              restaurant.newCustomerReturn / (data.booking - data.cancel) * 100,
        ),
        _buildChartWithInfo(
          title: "Khách Cũ Trở Lại",
          sub: restaurant.oldCustomerReturn,
          color: AppColors.DEFAULT_WARRING,
          icon: Icon(
            Icons.arrow_upward,
            color: AppColors.DEFAULT_INFO,
            size: 20,
          ),
          percent:
              restaurant.oldCustomerReturn / (data.booking - data.cancel) * 100,
        ),
      ],
    );
  }

  Widget _buildChartWithInfo(
      {String title, int sub, Color color, Icon icon, double percent}) {
    const start = CrossAxisAlignment.start;
    const baseline = CrossAxisAlignment.baseline;
    const center = MainAxisAlignment.center;
    return AspectRatio(
      aspectRatio: 2.15,
      child: Container(
        margin: const EdgeInsets.all(4),
        padding: const EdgeInsets.symmetric(vertical: 8),
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: const BorderRadius.all(Radius.circular(10))),
        child: Row(
          crossAxisAlignment: baseline,
          children: [
            Expanded(
              flex: 4,
              child: AppCustomChart(
                title: title,
                colorThisWeek: color,
                radito: 1.1,
                subTitle: sub,
              ),
            ),
            Expanded(
              flex: 3,
              child: Column(
                crossAxisAlignment: start,
                mainAxisAlignment: center,
                children: [
                  Expanded(
                    flex: 11,
                    child: Row(
                      children: [
                        Opacity(
                          opacity: 0,
                          child: icon,
                        ),
                        Column(
                          crossAxisAlignment: start,
                          children: [
                            Text(
                              "${percent.toStringAsFixed(0)}%",
                              style: AppStyles.DEFAULT_MEDIUM_BOLD,
                            ),
                            Text(
                              "Tổng Số Khách",
                              style: AppStyles.DEFAULT_SMALL_BOLD
                                  .copyWith(color: Colors.grey),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: Row(
                      crossAxisAlignment: start,
                      children: [
                        icon,
                        Column(
                          crossAxisAlignment: start,
                          children: [
                            Text(
                              "5%",
                              style: AppStyles.DEFAULT_MEDIUM_BOLD,
                            ),
                            Text(
                              "Tuần Trước",
                              style: AppStyles.DEFAULT_SMALL_BOLD
                                  .copyWith(color: Colors.grey),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListCustomer(List<Customer> list) {
    list.sort((a, b) => b.paid.compareTo(a.paid));
    List<Widget> listWg = new List<Widget>();
    for (int i = 0; i < 5; i++) {
      listWg.add(_buildRowCustomer(list[i], i, i == 0 ? 5 : 7));
    }
    return Wrap(
      runSpacing: 16,
      children: [
        Text(
          "Khách Hàng Chi Tiêu Nhiều Nhất",
          style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.white),
        ),
        Wrap(
          runSpacing: 4,
          children: listWg,
        )
      ],
    );
  }

  Widget _buildRowCustomer(Customer customer, int index, double radito) {
    double left = index == 0 ? 1 : 7 / 5;
    var styleNum = index == 0
        ? (AppStyles.DEFAULT_VERY_LARGE_BOLD.copyWith(color: Colors.white))
        : (AppStyles.DEFAULT_LARGE_BOLD.copyWith(color: Colors.white));
    Color color = index == 0 ? AppColors.DEFAULT_DANGER : Colors.black54;
    var small = AppStyles.DEFAULT_SMALL.copyWith(color: Colors.grey);
    var medium = AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: color);
    return Container(
      margin: const EdgeInsets.only(bottom: 4),
      decoration: const BoxDecoration(
          color: Colors.grey,
          borderRadius: const BorderRadius.all(Radius.circular(10))),
      child: AspectRatio(
        aspectRatio: radito,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            AspectRatio(
              aspectRatio: left,
              child: Container(
                child: Center(
                    child: Text(
                  (index + 1).toString(),
                  style: styleNum,
                )),
                decoration: BoxDecoration(
                    color: color,
                    borderRadius: const BorderRadius.horizontal(
                        left: Radius.circular(10))),
              ),
            ),
            AspectRatio(
              aspectRatio: radito - left,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      const BorderRadius.horizontal(right: Radius.circular(10)),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          customer.name.toUpperCase(),
                          style: medium,
                        ),
                        Text(
                          customer.phoneNumber,
                          style: small,
                        )
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          customer.paid.toString(),
                          style: medium,
                        ),
                        Text(
                          "Triệu",
                          style: small,
                        )
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
