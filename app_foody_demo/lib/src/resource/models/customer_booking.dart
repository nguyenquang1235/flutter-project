import 'package:flutter/material.dart';

import '../resource.dart';

class CustomerBooking {
  Customer customer;
  Day timeBooking;
  TableStatus status;
  String codeBooking;
  Day area;
  Day level;
  List<TableDraw> tables;

  CustomerBooking(
      {@required this.customer,
      @required this.timeBooking,
      this.area,
      this.level,
      this.status,
      this.tables,
      this.codeBooking});

  String getTable() {
    String result = "";
    for (int i = 0; i < tables.length; i++) {
      i < tables.length - 1
          ? result += "${tables[i].id} - "
          : result += "${tables[i].id}";
    }
    return result;
  }
}
