import 'package:app_foody_demo/src/resource/models/customer.dart';
import 'package:flutter/cupertino.dart';

class Restaurant {
  int id;
  String name;
  int capacity;
  int newCustomer;
  int newCustomerReturn;
  int oldCustomerReturn;
  List<Customer> listCustomer;

  Restaurant({
    @required this.id,
    @required this.name,
    @required this.capacity,
    this.newCustomer,
    this.newCustomerReturn,
    this.oldCustomerReturn,
    this.listCustomer,
  });
}
