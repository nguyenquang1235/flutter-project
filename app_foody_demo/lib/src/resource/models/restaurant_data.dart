import 'package:flutter/cupertino.dart';

import '../resource.dart';

class RestaurantData {
  int restaurantId;
  Day day;
  int booking;
  int cancel;
  int hanoi;
  int toanquoc;
  int vip1;
  int vip2;
  int vip3;
  int male;
  int female;
  int ageUnder18;
  int age18_25;
  int age25_35;
  int age35_50;
  int ageOver50;

  RestaurantData(
      {@required this.restaurantId,
      this.day,
      this.booking,
      this.cancel,
      this.hanoi,
      this.toanquoc,
      this.vip1,
      this.vip2,
      this.vip3,
      this.male,
      this.female,
      this.ageUnder18,
      this.age18_25,
      this.age25_35,
      this.age35_50,
      this.ageOver50});
}
