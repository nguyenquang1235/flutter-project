import 'package:app_foody_demo/src/resource/resource.dart';
import 'package:flutter/material.dart';
import 'package:app_foody_demo/src/configs/configs.dart';

class CustomSizeBox extends StatelessWidget {
  final ParamFunc change;
  final TextEditingController controller;
  final String hintText;
  final double width;
  final double height;
  final surfix;
  final bool enable;
  CustomSizeBox(this.controller, this.hintText, this.enable,
      {this.height, this.width, this.surfix, this.change});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(30)),
        boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 10)],
        // border:Border.all(width: 1, color: )
      ),
      child: SizedBox(
        child: TextFormField(
          enabled: enable,
          controller: controller,
          onChanged: (value){
            change(value);
          },
          textAlignVertical: TextAlignVertical.bottom,
          decoration: InputDecoration(
              disabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  borderSide: BorderSide(width: 1, color: Colors.white)),
              suffixIcon: surfix,
              fillColor: Colors.white,
              filled: true,
              hintText: hintText,
              hintStyle: AppStyles.DEFAULT_MEDIUM
                  .copyWith(color: AppColors.DEFAULT_DARK),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  borderSide: BorderSide(width: 1, color: Colors.white)),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  borderSide: BorderSide(width: 1, color: Colors.white))),
        ),
      ),
    );
  }
}
