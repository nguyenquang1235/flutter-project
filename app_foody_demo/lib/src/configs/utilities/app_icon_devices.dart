import 'package:flutter/material.dart';
import 'package:app_foody_demo/src/configs/configs.dart';
class AppIconDevices{
  BuildContext _context;
  Size SizeDevice;
  String BgImage1 = AppImages.BG_IMAGE_X1;
  String Logo = AppImages.LOGO_X1;
  String Component = AppImages.COMPONENT_X1;
  String Chef = AppImages.CHEF_X1;
  String FingerPrint = AppImages.FINGERPRINT_X1;
  String BgGreen = AppImages.BG_GREEN_X1;
  String FaceBook = AppImages.FACEBOOK_X1;
  String Zalo = AppImages.ZALO_X1;
  String Group = AppImages.GROUP_X1;
  String CheckSecondary = AppImages.CHECK_SECONDARY_X1;
  String CheckPrimary = AppImages.CHECK_PRIMARY_X1;
  String CheckWarring = AppImages.CHECK_WARRING_X1;
  String CheckDanger = AppImages.CHECK_DANGER_X1;
  String IconArrow = AppImages.ICON_ARROW_X1;
  String IconBackArrow = AppImages.ICON_BACK_ARROW_X1;
  String IconMenu = AppImages.ICON_MENU_X1;
  String IconEye = AppImages.ICON_VIEW_EYE_X1;
  AppIconDevices(this._context){
    SizeDevice = MediaQuery.of(_context).size;
    _getImageforDevice();
  }
  _getImageforDevice(){
    if(SizeDevice.width > 500){
      BgImage1 = AppImages.BG_IMAGE_X2;
      Logo = AppImages.LOGO_X2;
      Component = AppImages.COMPONENT_X2;
      Chef = AppImages.CHEF_X2;
     FingerPrint = AppImages.FINGERPRINT_X2;
      BgGreen = AppImages.BG_GREEN_X2;
      FaceBook = AppImages.FACEBOOK_X2;
      Zalo = AppImages.ZALO_X2;
      Group = AppImages.GROUP_X2;
      CheckSecondary = AppImages.CHECK_SECONDARY_X2;
      CheckPrimary = AppImages.CHECK_PRIMARY_X2;
      CheckWarring = AppImages.CHECK_WARRING_X2;
      CheckDanger = AppImages.CHECK_DANGER_X2;
      IconArrow = AppImages.ICON_ARROW_X2;
      IconBackArrow = AppImages.ICON_BACK_ARROW_X2;
      IconMenu = AppImages.ICON_MENU_X2;
      IconEye = AppImages.ICON_VIEW_EYE_X2;
    }
  }
}