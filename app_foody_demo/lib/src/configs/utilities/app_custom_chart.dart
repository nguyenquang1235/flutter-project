import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:app_foody_demo/src/configs/constanst/constansts.dart';

class AppCustomChart extends StatefulWidget {
  final String title;
  final int subTitle;
  // final List listData;
  final double radito;
  final Color colorThisWeek;
  final Color colorLastWeek = AppColors.DEFAULT_GREY;

  @override
  _AppCustomChartState createState() => _AppCustomChartState();
  AppCustomChart({
    @required this.title,
    @required this.colorThisWeek,
    // @required this.listData,
    @required this.radito,
    this.subTitle,
  });
}

class _AppCustomChartState extends State<AppCustomChart> {
  final titleStyle =
      AppStyles.DEFAULT_SMALL_BOLD.copyWith(color: Colors.grey);
  final subtitleStyle = AppStyles.DEFAULT_MEDIUM_BOLD
      .copyWith(color: AppColors.DEFAULT_DARK, letterSpacing: 1);
  final childStyle = AppStyles.DEFAULT_VERY_SMALL_BOLD
      .copyWith(color: Colors.grey);
  final double width = 4;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: widget.radito,
      child: Card(
        elevation: 0,
        color: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: _buildChartBody(),
      ),
    );
  }

  Widget _buildChartBody() {
    return Padding(
      padding: const EdgeInsets.all(6),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            flex: 2,
            child: Text(
              widget.title,
              style: titleStyle,
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              widget.subTitle == null?"":"${widget.subTitle}",
              style: subtitleStyle,
            ),
          ),
          Expanded(
            flex: 8,
            child: _buildChartData(),
          ),
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                      width: 10,
                      height: 10,
                      child: CircleAvatar(
                        backgroundColor: widget.colorThisWeek,
                      ),
                    ),
                    SizedBox(
                      width: 3,
                    ),
                    Text(
                      "TUẦN NÀY",
                      style: childStyle,
                    )
                  ],
                ),
                Row(
                  children: [
                    Container(
                      width: 10,
                      height: 10,
                      child: CircleAvatar(
                        backgroundColor: widget.colorLastWeek,
                      ),
                    ),
                    SizedBox(
                      width: 3,
                    ),
                    Text(
                      "TUẦN TRƯỚC",
                      style: childStyle,
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildChartData() {
    return Container(
      child: BarChart(
        BarChartData(
          barGroups: _buildBarGroups(),
          titlesData: _buildFlTitleData(),
          alignment: BarChartAlignment.spaceEvenly,
          borderData: FlBorderData(
            show: false,
          ),
        ),
      ),
    );
  }

  BarChartGroupData _makeGroupData(int x, int y1, int y2) {
    return BarChartGroupData(
      barsSpace: 4,
      x: x,
      barRods: [
        BarChartRodData(
          y: double.parse(y1.toString()),
          colors: [widget.colorThisWeek],
          width: width,
        ),
        BarChartRodData(
          y: double.parse(y2.toString()),
          colors: [widget.colorLastWeek],
          width: width,
        )
      ],
    );
  }

  List<BarChartGroupData> _buildBarGroups() {
    List<BarChartGroupData> list = new List<BarChartGroupData>();
    var rnd = new Random();
    for (int i = 0; i < 6; i++) {
      list.add(
        _makeGroupData(
          i,
          3 + rnd.nextInt(21 - 3),
          3 + rnd.nextInt(21 - 3),
        ),
      );
    }
    return list;
  }

  FlTitlesData _buildFlTitleData() {
    return FlTitlesData(
      show: true,
      bottomTitles: SideTitles(
        showTitles: true,
        getTextStyles: (value) => childStyle,
        margin: 0,
        getTitles: (double value) {
          switch (value.toInt()) {
            case 0:
              return 'T2';
            case 1:
              return 'T3';
            case 2:
              return 'T4';
            case 3:
              return 'T5';
            case 4:
              return 'T6';
            case 5:
              return 'T7';
            case 6:
              return 'CN';
            default:
              return '';
          }
        },
      ),
      leftTitles: SideTitles(
        showTitles: false,
      ),
    );
  }
}
