import 'package:flutter/material.dart';

class AppColors{
  AppColors._();
  static const DEFAULT_PRIMARY = Color.fromARGB(250, 38, 117, 30);
  static const DEFAULT_SECONDARY = Color.fromARGB(250, 94, 85, 220);
  static const DEFAULT_INFO = Color.fromARGB(250, 12, 180, 79);
  static const DEFAULT_DANGER = Color.fromARGB(250, 210, 31, 45);
  static const DEFAULT_WARRING = Color.fromARGB(250, 216, 183, 30);
  static const DEFAULT_DARK = Color.fromARGB(250, 28, 23, 41);
  static const DEFAULT_LIGHT = Color.fromARGB(250, 241, 231, 246);
  static const DEFAULT_GREY = Color.fromARGB(100, 127, 121, 131);
}
