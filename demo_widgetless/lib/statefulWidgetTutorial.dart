import 'dart:ffi';

import 'package:flutter/material.dart';
import 'employeeDetails.dart';
import 'Employee.dart';
import 'CreateEmployee.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: "StatefulWidget Tutorial",
      home: MyHome(),
    );
  }
}

class MyHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.menu),
          tooltip: "Navigation Menu",
          onPressed: null,
          iconSize: 30.0,
        ),
        title: Center(
          child:
          Text("Employee Manager", style: const TextStyle(color: Colors.black)),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            // onPressed: () =>  Navigator.push(
            //   context,
            //   MaterialPageRoute(
            //     builder: (context) => CreateEmployee,
            //   ),
            // ),
            tooltip: "Search",
            iconSize: 40.0,
          ),
        ],
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 16.0),
        child: MyClass(),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.green,
        child: Icon(Icons.add),
        tooltip: "Add new employee",
        onPressed: () =>  Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CreateEmployee(),
          ),
        ),
      ),
    );
  }
}

class MyClass extends StatefulWidget {
  @override
  MyClassState createState() => new MyClassState();
}

class MyClassState extends State<MyClass> {
  List<Employee> ListEmp;
  Set<Employee>
      listLiked; //Set không cho tồn tại 2 giá trị giống nhau trong tập hợp
  MyClassState() {
    listLiked = new Set<Employee>();
    ListEmp = [
      Employee.tao(1, "Nguyễn Đăng Quang", "Developer", "(84) 855 948 455",
          "37 Bùi Thị Xuân, Thành phố Huế, Tỉnh Thừa-Thiên Huế", "Nam"),
      Employee.tao(2, "Lê Thị Kim Trâm", "Developer", "(84) 944 081 535",
          "Thị xã Hương Thủy, Tỉnh Thừa-Thiên Huế", "Nữ"),
      Employee.tao(3, "Nguyen Ngoc Quyen", "Developer", "(84) 123 456 789",
          "Thành phố Huế, Tỉnh Thừa Thiên Huế", "Nam"),
      Employee.tao(4, "Nguyen Xuan Trung", "Developer", "(84) 869 938 936",
          "Thành phố Huế, Tỉnh Thừa Thiên Huế", "Nam"),
      Employee.tao(5, "Tran Van Minh", "Developer", "(84) 986 622 332",
          "Thành phố Huế, Tỉnh Thừa Thiên Huế", "Nam"),
      Employee.tao(6, "Nguyen Thi Ghi Ghi", "Hr", "(84) 986 622 332",
          "Thành phố Huế, Tỉnh Thừa Thiên Huế", "Nam"),
      Employee.tao(7, "Nguyen Thanh Vinh", "TeamLead", "(84) 986 622 332",
          "Thành phố Huế, Tỉnh Thừa Thiên Huế", "Nam"),
      Employee.tao(8, "Le Dinh Vu", "TeamLead", "(84) 986 622 332",
          "Thành phố Huế, Tỉnh Thừa Thiên Huế", "Nam"),
      Employee.tao(9, "Bui Cong Dat", "TeamLead", "(84) 986 622 332",
          "Thành phố Huế, Tỉnh Thừa Thiên Huế", "Nam")
    ];
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView.builder(
        itemCount: ListEmp.length,
        itemBuilder: (context, index) {
          return buildRow(ListEmp[index], index);
        });
  }

  Widget buildRow(Employee employee, int i) {
    final bool saved = listLiked.contains(employee);
    return Column(
      children: [
        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 12,vertical: 12),
          leading: Container(
            padding: EdgeInsets.symmetric(horizontal: 12,vertical: 12),
            child: Icon(Icons.person,color: Colors.white, size: 30),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(100)),
              color: Colors.grey[300]
            ),
          ),
          title: Text("${employee.Name}\n${employee.PhoneNumber}",
              style: const TextStyle(
                  fontWeight: FontWeight.normal)),
          trailing: IconButton(
            icon: ChangeFavorColor(saved),
            tooltip: "Favorite",
            onPressed: () =>
                _AddFavorList(employee, saved), //Sử dụng để like nhân viên
            // cú pháp [funtion giả] return về funtion event;
            iconSize: 30,
            // visualDensity: VisualDensity.lerp(VisualDensity.compact, VisualDensity.compact, 100),
          ),
          onTap: () => _ViewEmployeeDetails(
              employee), //Sử dụng để xem thong tin nhân viên
        ),
      ],
    );
  }

  Widget ChangeFavorColor(bool saved) {
    if (saved)
      return Icon(
        Icons.favorite,
        color: Colors.red,
      );
    return Icon(
      Icons.favorite,
      color: null,
    );
  }

  void _AddFavorList(Employee employee, bool saved) {
    setState(() {
      if (saved) {
        listLiked.remove(employee);
      } else {
        listLiked.add(employee);
      }
    });
  }

  void _ViewEmployeeDetails(Employee employee) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EmployeeDetailsHome(employee),
      ),
    );
  }
}
