
import 'package:flutter/material.dart';

import 'Employee.dart';
import 'statefulWidgetTutorial.dart';

class CreateEmployee extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: "Create employee",
      home: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.close),
            tooltip: "Back",
            onPressed: null,
          ),
          actions: [
            IconButton(
              icon: Icon(Icons.check),
              onPressed: null,
              tooltip: "Finish",
            )
          ],
          title: Center(
            child: Text("Create New Employee", style: TextStyle(color: Colors.black),),
          ),
        ),
        body: Container(
              alignment: Alignment(0, 1),
              color: Colors.green,
              // decoration: BoxDecoration(
              //   image: DecorationImage(
              //     fit: BoxFit.fill,
              //   ),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      alignment: Alignment(0,0),
                      padding: EdgeInsets.symmetric(vertical: 32),
                      margin: EdgeInsets.only(left: 122.5, right: 122.5, bottom: 160),
                      child: Icon(Icons.person,color: Colors.white, size: 50),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(100)),
                          color: Colors.grey[300]
                      ),
                    ),
                    Container(
                      // height: 340,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                      // margin: EdgeInsets.only(top: 300),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text("PHONE NUMBER",
                              style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.blueAccent,
                                  fontWeight: FontWeight.bold)),
                          TextField(),
                          Text("FULL NAME",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 15)),
                          Text("",
                              style: TextStyle(fontSize: 15, color: Colors.grey)),
                          Divider(),
                          Text("GENDER",
                              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
                          Text("",
                              style: TextStyle(fontSize: 15, color: Colors.grey)),
                          Divider(),
                          Text("ADDRESS",
                              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
                          Text("",
                              style: TextStyle(fontSize: 15, color: Colors.grey)),
                          Divider(),
                          Text("DEPARTMENT",
                              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
                          Text("",
                              style: TextStyle(fontSize: 15, color: Colors.grey)),
                        ],
                      ),
                    )
                  ],
                ),
              ) /* add child content chere */,
            )
        // body: Container(
        //   margin: EdgeInsets.symmetric(horizontal: 16),
        //   alignment: Alignment(0,1),
        //   child: ListView(
        //     // mainAxisSize: MainAxisSize.max,
        //     // mainAxisAlignment: MainAxisAlignment.spaceAround,
        //     // crossAxisAlignment: CrossAxisAlignment.start,
        //     children: [
        //       Container(
        //         alignment: Alignment(0,0),
        //         padding: EdgeInsets.symmetric(vertical: 32),
        //         margin: EdgeInsets.symmetric(horizontal: 105,vertical: 16),
        //         child: Icon(Icons.person,color: Colors.white, size: 50),
        //         decoration: BoxDecoration(
        //             borderRadius: BorderRadius.all(Radius.circular(100)),
        //             color: Colors.grey[300]
        //         ),
        //       ),
        //       Text("Full Name ", style: TextStyle(fontSize: 15, height: 1)),
        //       Divider(),
        //       TextField(
        //         cursorHeight: 20,
        //         textCapitalization: TextCapitalization.sentences,
        //         decoration: InputDecoration(
        //             enabledBorder: OutlineInputBorder(
        //                 borderRadius: BorderRadius.all(Radius.circular(15)),
        //                 borderSide: BorderSide(width: 1.5)
        //             ),
        //             focusedBorder: OutlineInputBorder(
        //                 borderRadius: BorderRadius.all(Radius.circular(15)),
        //                 borderSide: BorderSide(width: 1.5,color: Colors.blue)
        //             ),
        //         ),
        //       ),
        //       Text("Full Name: "),
        //       Divider(),
        //       TextField(
        //         cursorHeight: 20,
        //         textCapitalization: TextCapitalization.sentences,
        //         decoration: InputDecoration(
        //             enabledBorder: OutlineInputBorder(
        //                 borderRadius: BorderRadius.all(Radius.circular(15)),
        //                 borderSide: BorderSide(width: 1.5)
        //             ),
        //             focusedBorder: OutlineInputBorder(
        //                 borderRadius: BorderRadius.all(Radius.circular(15)),
        //                 borderSide: BorderSide(width: 1.5,color: Colors.blue)
        //             ),
        //             hintText: "Phone Number",
        //             hintStyle: TextStyle(fontSize: 15)
        //         ),
        //       ),
        //       Text("Full Name: "),
        //       Divider(),
        //       TextField(
        //         cursorHeight: 20,
        //         textCapitalization: TextCapitalization.sentences,
        //         decoration: InputDecoration(
        //             enabledBorder: OutlineInputBorder(
        //                 borderRadius: BorderRadius.all(Radius.circular(15)),
        //                 borderSide: BorderSide(width: 1.5)
        //             ),
        //             focusedBorder: OutlineInputBorder(
        //                 borderRadius: BorderRadius.all(Radius.circular(15)),
        //                 borderSide: BorderSide(width: 1.5,color: Colors.blue)
        //             ),
        //             hintText: "Gender",
        //             hintStyle: TextStyle(fontSize: 15)
        //         ),
        //       ),
        //       Text("Full Name: "),
        //       Divider(),
        //       TextField(
        //         cursorHeight: 20,
        //         textCapitalization: TextCapitalization.sentences,
        //         decoration: InputDecoration(
        //             enabledBorder: OutlineInputBorder(
        //                 borderRadius: BorderRadius.all(Radius.circular(15)),
        //                 borderSide: BorderSide(width: 1.5)
        //             ),
        //             focusedBorder: OutlineInputBorder(
        //                 borderRadius: BorderRadius.all(Radius.circular(15)),
        //                 borderSide: BorderSide(width: 1.5,color: Colors.blue)
        //             ),
        //             hintText: "Department",
        //             hintStyle: TextStyle(fontSize: 15)
        //         ),
        //       ),
        //       Text("Full Name: "),
        //       Divider(),
        //       TextField(
        //         maxLines: 2,
        //         cursorHeight: 20,
        //         textCapitalization: TextCapitalization.sentences,
        //         decoration: InputDecoration(
        //             enabledBorder: OutlineInputBorder(
        //                 borderRadius: BorderRadius.all(Radius.circular(15)),
        //                 borderSide: BorderSide(width: 1.5)
        //             ),
        //             focusedBorder: OutlineInputBorder(
        //                 borderRadius: BorderRadius.all(Radius.circular(15)),
        //                 borderSide: BorderSide(width: 1.5,color: Colors.blue)
        //             ),
        //             hintText: "Address",
        //             hintStyle: TextStyle(fontSize: 15)
        //         ),
        //       )
        //     ],
        //   ),
        // ),
      ),
    );
  }
}

