import 'dart:ffi';

import 'package:flutter/material.dart';

void main(){
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: "Flutter tutorial",
      home: TutorialHome(),
    );
  }
}


class TutorialHome extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: null,
          tooltip: "Navigation Menu",
        ),
        title: Center(
          child: Text("Goldden Booking"),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: null,
            tooltip: "Navigation Menu",
          ),
        ],
      ),
      body: Center(
        child: Text("Hello world"),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        tooltip: "Add",
        onPressed: null,
      ),
    );
  }
}