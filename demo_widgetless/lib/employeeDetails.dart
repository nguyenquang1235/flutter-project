import 'dart:ui';

import 'Employee.dart';
import 'package:flutter/material.dart';

class EmployeeDetailsHome extends StatelessWidget {
  Employee employee;
  EmployeeDetailsHome(this.employee);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Employee Details",
      home: Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: Color.fromARGB(235, 248, 248, 248),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Center(
            child: Text("${employee.Name}"),
          ),
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              tooltip: "Back",
              onPressed: null //Trở về trang quản lý nhân viên
              ),
          actions: [
            IconButton(
                icon: Icon(Icons.menu_outlined),
                tooltip: "Menu",
                onPressed: null //mở các tác vụ như chỉnh sửa và xóa nhân viên
                )
          ],
          // leadingWidth: 8,
        ),
        body: EmployeeDetails(employee)
      ),
    );
  }
}

class EmployeeDetails extends StatefulWidget {
  Employee employee;
  @override
  EmployeeDetails(Employee employee){
     this.employee = employee;
  }
  EmployeeDetailsState createState() => new EmployeeDetailsState();
}

class EmployeeDetailsState extends State<EmployeeDetails> {
  String srcAvatar;
  EmployeeDetailsState() {
    // cach tao khac
    // (Employee q){
    //   q.Name = "QUang";
    // };
    this.srcAvatar = "assets/img/avatar3.jpg";
  }
  @override
  Widget build(BuildContext context) {
    final Employee emp = widget.employee;
    return Container(
      alignment: Alignment(0, 1),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(srcAvatar),
          fit: BoxFit.fill,
        ),
      ),
      child: Container(
        // height: 340,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
        ),
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        // margin: EdgeInsets.only(top: 300),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text("PHONE NUMBER",
                style: TextStyle(
                    fontSize: 15,
                    color: Colors.blueAccent,
                    fontWeight: FontWeight.bold)),
            Divider(
              color: Colors.blueAccent,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("${emp.PhoneNumber}",
                    style: TextStyle(fontSize: 20, color: Colors.grey)),
                Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          Icons.phone,
                          color: Colors.blue,
                        ),
                        tooltip: "Call",
                        onPressed: null),
                    IconButton(
                        icon: Icon(
                            Icons.message,
                            color: Colors.blue),
                        tooltip: "Message",
                        onPressed: null)
                  ],
                )
              ],
            ),
            Text("FULL NAME",
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 15)),
            Text("${emp.Name}",
                style: TextStyle(fontSize: 15, color: Colors.grey)),
            Divider(),
            Text("GENDER",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
            Text("${emp.Gender}",
                style: TextStyle(fontSize: 15, color: Colors.grey)),
            Divider(),
            Text("ADDRESS",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
            Text("${emp.Address}",
                style: TextStyle(fontSize: 15, color: Colors.grey)),
            Divider(),
            Text("DEPARTMENT",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
            Text("${emp.Department}",
                style: TextStyle(fontSize: 15, color: Colors.grey)),
          ],
        ),
      ) /* add child content here */,
    );// Hiển thị thông tin nhân viên
  }
}
