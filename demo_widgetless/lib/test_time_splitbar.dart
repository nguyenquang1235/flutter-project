import 'dart:async';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with TickerProviderStateMixin {
  AnimationController animationController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    animationController = new AnimationController(
        vsync: this, duration: const Duration(seconds: 30));
    // animationController.repeat(reverse: true);
    animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    Animation<double> animation =
        CurvedAnimation(curve: Curves.easeIn, parent: animationController);

    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          color: Colors.black54,
          width: 350,
          child: Center(
            child: AnimatedBuilder(
              builder: (context, child) {
                return Transform.rotate(
                  angle: pi,
                  child: Container(
                    // margin: EdgeInsets.all(2),
                    padding: EdgeInsets.symmetric(horizontal: 2.5, vertical: 1),
                    height: 10,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border: Border.all(color: Colors.white, width: 1),
                    ),
                    child: LinearProgressIndicator(
                      value: 1-animation.value,
                      backgroundColor: Colors.white,
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
                    ),
                  ),
                );
              },
              animation: animation,
            ),
          ),
        ),
      ),
    );
  }
}
