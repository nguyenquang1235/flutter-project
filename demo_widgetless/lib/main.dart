
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with TickerProviderStateMixin {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Test(),
      ),
    );
  }
}

class Test extends StatefulWidget {
  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Test> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Center(
      child: Stack(
        children: [
          _buildFace(),
          _buildHand()
        ],
      ),
    );
  }

  _buildFace(){
    return Center(
      child: Container(
        width: SizeConfig.blockSizeHorizontal*55,
        height: SizeConfig.blockSizeHorizontal*55,
        child: Image.asset("assets/img/victoryface.png", fit: BoxFit.fill,),
      ),
    );
  }
  
  _buildHand(){
    print(SizeConfig.screenHeight);
    print(SizeConfig.blockSizeVertical);
    return Align(
      alignment: Alignment(-0.75,-0.25),
      child: Container(
        width: SizeConfig.blockSizeHorizontal*30,
        height: SizeConfig.blockSizeHorizontal*40,
        child: Image.asset("assets/img/victoryhand.png", fit: BoxFit.fill,),
      ),
    );
  }
}


class SizeConfig {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double blockSizeHorizontal;
  static double blockSizeVertical;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;
  }
}
