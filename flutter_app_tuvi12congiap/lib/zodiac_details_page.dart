
import 'package:flutter/cupertino.dart';
import 'package:flutter_app_tuvi12congiap/src/resource/model/zodiac_date.dart';

import 'src/configs/constanst/constants.dart';
import 'package:flutter/material.dart';

import 'src/resource/model/models.dart';

class ZodiacDetailsPage extends StatelessWidget {
  List<Zodiac> ListZodiac;
  int IndexOfItem;
  ZodiacDetailsPage(this.ListZodiac, this.IndexOfItem);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: MyAppBar.createDefaultAppbar(
            "tử vi 12 chòm sao",
            MyAppBar.createBackBtn(context)
          ),
          bottomNavigationBar: MyAppBar.createDefaultBottom(context),
          body: Stack(
                children: [
                  Container(
                    height: 75,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/images/bg2.png"),
                        fit: BoxFit.cover
                      )
                    ),
                  ),
                  DefaultTabController(
                    initialIndex: IndexOfItem,
                    length: ListZodiac.length,
                    child: Scaffold(
                        backgroundColor: Colors.transparent,
                        appBar: AppBar(
                          toolbarHeight: 80,
                          bottom: buildTabBar(context),
                          backgroundColor: Colors.transparent,
                          elevation: 0,
                        ),
                        body: TabBarView(
                          children: buildAboutTabView(),
                        )
                    ),
                  )
                ],
              )
      ),
    );
  }

  List<Widget> buildAboutTabbar() {
    List<Tab> listWidget = <Tab>[];
    for (var i = 0; i < ListZodiac.length; i++) {
      listWidget.add(Tab(
        icon:Image.asset("${ListZodiac[i].AvatarPath}",width: 46,)
      ));
    }
    return listWidget;
  }

  List<Widget> buildAboutTabView(){
    List<Widget> listWidget = <Widget>[];
    for (var i = 0; i < ListZodiac.length; i++) {
      listWidget.add(buildBody(ListZodiac[i]));
    }
    return listWidget;
  }

  Widget buildTabBar(BuildContext context) {
    var DeviceSize = MediaQuery.of(context).size;
    double horizontal = (DeviceSize.width - 46*6)/12;
    return TabBar(
      indicator: TriangleTabIndicator(color: Colors.white),
      labelPadding: EdgeInsets.symmetric(vertical: 16,horizontal: horizontal),
      isScrollable: true,
      tabs: buildAboutTabbar(),
    );
  }

  Widget buildBody(Zodiac _zodiac) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: ListView(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Text("${_zodiac.Name}",
                      style: AppStyles.DEFAULT_LARGE_BOLD
                          .copyWith(color: AppColors.primary)),
                  Text("${_zodiac.DateStart}- ${_zodiac.DateEnd}",
                      style: AppStyles.DEFAULT_LARGE
                          .copyWith(color: AppColors.primary))
                ],
              ),
              Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(8),
                    child: Image.asset("assets/images/daychuyen.png",width: 30, height: 30, fit: BoxFit.fill),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        border: Border.all(color: Colors.blueAccent, width: 4)),
                  )
                ],
              )
            ],
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: Text(
              "${_zodiac.Description}",
              textAlign: TextAlign.justify,
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Con số may mắn: ${_zodiac.LuckyNumber}",
                    style: TextStyle(color: AppColors.link)),
                Text("Màu sắc may mắn: ${_zodiac.LuckyColor}",
                    style: TextStyle(color: AppColors.link))
              ],
            ),
          )
        ],
      ),
    );
  }
}

class TriangleTabIndicator extends Decoration {
  final BoxPainter _painter;

  TriangleTabIndicator({@required Color color, @required double radius})
      : _painter = DrawTriangle(color);

  @override
  BoxPainter createBoxPainter([onChanged]) => _painter;
}

class DrawTriangle extends BoxPainter {
  Paint _paint;

  DrawTriangle(Color color) {
    _paint = Paint()
      ..color = color
      ..style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration cfg) {
    final Offset triangleOffset =
        offset + Offset(cfg.size.width / 2, cfg.size.height - 10);
    var path = Path();

    path.moveTo(triangleOffset.dx, triangleOffset.dy);
    path.lineTo(triangleOffset.dx + 10, triangleOffset.dy + 10);
    path.lineTo(triangleOffset.dx - 10, triangleOffset.dy + 10);

    path.close();
    canvas.drawPath(path, _paint);
  }
}
