import 'package:flutter/material.dart';
import 'package:flutter_app_tuvi12congiap/src/configs/configs.dart';
import 'package:flutter_app_tuvi12congiap/src/resource/model/models.dart';

class UtilitiesMenu extends StatelessWidget{
  final String Ava = "assets/images/";
  List<Utility> ListUtility;
  UtilitiesMenu(){
    ListUtility = [
      Utility(1, Ava+"btn_utils_clicked_1.png", "Đổi ngày âm dương"),
      Utility(2, Ava+"btn_utils_clicked_2.png", "Xem tuổi vợ chồng"),
      Utility(3, Ava+"btn_utils_clicked_3.png", "Xem hướng nhà tốt"),
      Utility(4, Ava+"btn_utils_clicked_4.png", "Xem ngày tốt"),
      Utility(5, Ava+"btn_utils_clicked_5.png", "Tử vi trọn đời"),
      Utility(6, Ava+"btn_utils_clicked_6.png", "Tổng hợp văn khấn"),
      Utility(7, Ava+"btn_utils_clicked_7.png", "Nhịp sinh học"),
      Utility(8, Ava+"btn_utils_clicked_8.png", "Xem sao giải hạn"),
      Utility(9, Ava+"btn_utils_clicked_9.png", "Bói tình yêu"),
      Utility(10, Ava+"btn_utils_clicked_10.png", "12 Cung hoàng đạo"),
      Utility(11, Ava+"btn_utils_clicked_11.png", "Ứng dụng khác"),
      Utility(12, Ava+"btn_utils_clicked_12.png", "Chia sẻ")
    ];
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Utilities Menu",
      home: Scaffold(
        appBar: MyAppBar.createDefaultAppbar("tiện ích", MyAppBar.createBackBtn(context)),
        bottomNavigationBar: MyAppBar.createDefaultBottom(context),
        body: buildGridView(context),
      ),
    );
  }
  Widget buildGridView(BuildContext context) {
    return GridView.builder(
        itemCount: ListUtility.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 4, childAspectRatio: 10 / 30),
        itemBuilder: (context, index) {
          return buildItem(ListUtility[index]); // Build một item
        });
  }

  Widget buildItem(Utility utility) {
    return GestureDetector(
      child: Column(
        children: [
          Image.asset(
            "${utility.AvaPath}",
            width: 90,
            height: 90,
          ),
          Text(
            "${utility.Name}",
            style: AppStyles.DEFAULT_MEDIUM_BOLD,
          ),
        ],
      ),
    );
  }
}