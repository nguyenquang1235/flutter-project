import 'package:flutter/cupertino.dart';
import 'package:flutter_app_tuvi12congiap/src/resource/model/zodiac_date.dart';

import 'src/configs/constanst/constants.dart';
import 'package:flutter/material.dart';

import 'zodiac_details_page.dart';
import 'src/resource/model/models.dart';

void main() {
  runApp(Application());
}

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: MyApp());
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.createDefaultAppbar(
          "tử vi 12 chòm sao", MyAppBar.createBackBtn(context)),
      bottomNavigationBar: MyAppBar.createDefaultBottom(context),
      body: ShowListZodiac(),
    );
  }
}

class ShowListZodiac extends StatefulWidget {
  @override
  ShowListZodiacState createState() => new ShowListZodiacState();
}

class ShowListZodiacState extends State<ShowListZodiac> {
  List<Zodiac> ListZodiac;
  final String Ava = "assets/images/";

  ShowListZodiacState() {
    ListZodiac = [
      new Zodiac(
          1,
          "Bạch Dương",
          ZodiacDate(21, 3),
          ZodiacDate(20, 4),
          Ava + "zodiac1.png",
          Ava + "zodiac1_clicked.png",
          DescriptionZodiac.DEFAULT_ZODIAC1,
          "đỏ thắm và cam",
          "1 và 9"),
      new Zodiac(
          2,
          "Kim Ngưu",
          ZodiacDate(21, 4),
          ZodiacDate(20, 5),
          Ava + "zodiac2.png",
          Ava + "zodiac2_clicked.png",
          DescriptionZodiac.DEFAULT_ZODIAC2,
          "đỏ thắm và cam",
          "1 và 9"),
      new Zodiac(
          3,
          "Song Tử",
          ZodiacDate(21, 5),
          ZodiacDate(21, 6),
          Ava + "zodiac3.png",
          Ava + "zodiac3_clicked.png",
          DescriptionZodiac.DEFAULT_ZODIAC3,
          "đỏ thắm và cam",
          "1 và 9"),
      new Zodiac(
          4,
          "Cự Giải",
          ZodiacDate(22, 6),
          ZodiacDate(22, 7),
          Ava + "zodiac4.png",
          Ava + "zodiac4_clicked.png",
          DescriptionZodiac.DEFAULT_ZODIAC4,
          "đỏ thắm và cam",
          "1 và 9"),
      new Zodiac(
          5,
          "Sư Tử",
          ZodiacDate(23, 7),
          ZodiacDate(22, 8),
          Ava + "zodiac5.png",
          Ava + "zodiac5_clicked.png",
          DescriptionZodiac.DEFAULT_ZODIAC5,
          "đỏ thắm và cam",
          "1 và 9"),
      new Zodiac(
          6,
          "Xử Nữ",
          ZodiacDate(23, 8),
          ZodiacDate(22, 9),
          Ava + "zodiac6.png",
          Ava + "zodiac6_clicked.png",
          DescriptionZodiac.DEFAULT_ZODIAC6,
          "đỏ thắm và cam",
          "1 và 9"),
      new Zodiac(
          7,
          "Thiên Bình",
          ZodiacDate(23, 9),
          ZodiacDate(23, 10),
          Ava + "zodiac7.png",
          Ava + "zodiac7_clicked.png",
          DescriptionZodiac.DEFAULT_ZODIAC7,
          "đỏ thắm và cam",
          "1 và 9"),
      new Zodiac(
          8,
          "Bọ Cạp",
          ZodiacDate(24, 10),
          ZodiacDate(22, 11),
          Ava + "zodiac8.png",
          Ava + "zodiac8_clicked.png",
          DescriptionZodiac.DEFAULT_ZODIAC8,
          "đỏ thắm và cam",
          "1 và 9"),
      new Zodiac(
          9,
          "Nhân Mã",
          ZodiacDate(23, 11),
          ZodiacDate(21, 12),
          Ava + "zodiac9.png",
          Ava + "zodiac9_clicked.png",
          DescriptionZodiac.DEFAULT_ZODIAC9,
          "đỏ thắm và cam",
          "1 và 9"),
      new Zodiac(
          10,
          "Ma Kết",
          ZodiacDate(22, 12),
          ZodiacDate(19, 1),
          Ava + "zodiac10.png",
          Ava + "zodiac10_clicked.png",
          DescriptionZodiac.DEFAULT_ZODIAC10,
          "đỏ thắm và cam",
          "1 và 9"),
      new Zodiac(
          11,
          "Bảo Bình",
          ZodiacDate(20, 1),
          ZodiacDate(18, 2),
          Ava + "zodiac11.png",
          Ava + "zodiac11_clicked.png",
          DescriptionZodiac.DEFAULT_ZODIAC11,
          "đỏ thắm và cam",
          "1 và 9"),
      new Zodiac(
          12,
          "Song Ngư",
          ZodiacDate(19, 2),
          ZodiacDate(20, 3),
          Ava + "zodiac12.png",
          Ava + "zodiac12_clicked.png",
          DescriptionZodiac.DEFAULT_ZODIAC12,
          "đỏ thắm và cam",
          "1 và 9"),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 8),
      child: buildGridView(context),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/images/bg2.png"), fit: BoxFit.fill)),
    );
  }

  Widget buildGridView(BuildContext context) {
    return GridView.builder(
        itemCount: ListZodiac.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3, childAspectRatio: 10 / 11.7),
        itemBuilder: (context, index) {
          return buildItem(ListZodiac[index]); // Build một item
        });
  }

  Widget buildItem(Zodiac zodiac) {
    return GestureDetector(
      child: Column(
        children: [
          Image.asset(
            "${zodiac.AvatarPath}",
            width: 90,
            height: 90,
          ),
          Text(
            "${zodiac.Name}",
            style: AppStyles.DEFAULT_MEDIUM_BOLD,
          ),
          Text("${zodiac.DateStart}- ${zodiac.DateEnd}")
        ],
      ),
      onTap: () => seeDetailsZodiac(ListZodiac.indexOf(zodiac)),
    );
  }

  void seeDetailsZodiac(int indexOfItem) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ZodiacDetailsPage(
              ListZodiac, indexOfItem)),
    );
  }
}
