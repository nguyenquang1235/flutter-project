import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static final Color primary = Color.fromRGBO(246, 133, 14, 1);
  static final Color grey = Colors.grey[300];
  static final Color black = Colors.black54;
  static final Color enableButton = Color.fromRGBO(246, 133, 14, 1);
  static final Color disableButton = Color.fromRGBO(246, 133, 14, 0.65);
  static final Color link = Color.fromARGB(250, 73, 101, 224);
}
