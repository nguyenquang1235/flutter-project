
import 'package:flutter/material.dart';
import 'constants.dart';
import 'package:flutter_app_tuvi12congiap/utilities_menu.dart';

class MyAppBar {
  static createBackBtn(BuildContext context) => GestureDetector(
    child: Container(
        padding: EdgeInsets.symmetric(vertical: 8),
        child: Image.asset("assets/images/backbtn.png")),
    onTap: () => Navigator.pop(context),
  );

  static createDefaultBottom(BuildContext context) => BottomAppBar(
    child: Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/images/bg.png"), fit: BoxFit.fill)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    Image.asset("assets/images/lichngay.png"),
                    Text(
                      "Lịch ngày",
                      style: AppStyles.DEFAULT_MEDIUM_GREY,
                    ),
                  ],
                ),
                Column(
                  children: [
                    Image.asset("assets/images/lichthang.png"),
                    Text("Lịch tháng", style: AppStyles.DEFAULT_MEDIUM_GREY),
                  ],
                ),
                Column(
                  children: [
                    Image.asset("assets/images/doingay.png"),
                    Text("Đổi ngày", style: AppStyles.DEFAULT_MEDIUM_GREY),
                  ],
                ),
                GestureDetector(
                  child: Column(
                    children: [
                      Image.asset("assets/images/tienich.png"),
                      Text("Tiện ích", style: AppStyles.DEFAULT_MEDIUM_GREY)
                    ],
                  ),
                  onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => UtilitiesMenu())),
                )
              ],
            ),
          )
        ],
      ),
    ),
  );

  static createDefaultAppbar(String title, Widget leading,{TabBar tabBar: null}) => AppBar(
        leading: leading,
        leadingWidth: 90,
        centerTitle: true,
        title: Text("${title}".toUpperCase()),
        bottom: tabBar,
        backgroundColor: Color.fromARGB(250, 73, 101, 224),
      );
}
