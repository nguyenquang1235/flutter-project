class ZodiacDate{
  int Day;
  int Month;
  ZodiacDate(this.Day, this.Month);

  @override
  String toString() {
    return "${Day}/${Month}";
  }
}