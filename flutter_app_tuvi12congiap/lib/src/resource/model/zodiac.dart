import 'zodiac_date.dart';

class Zodiac{
  int Id;
  String Name;
  ZodiacDate DateStart;
  ZodiacDate DateEnd;
  String AvatarPath;
  String AvatarClickedPath;
  String Description;
  String LuckyNumber;
  String LuckyColor;
  Zodiac(this.Id, this.Name, this.DateStart, this.DateEnd, this.AvatarPath, this.AvatarClickedPath, this.Description, this.LuckyColor, this.LuckyNumber);

}