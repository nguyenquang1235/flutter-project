import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Demo",
        home: Scaffold(
          backgroundColor: Color.fromARGB(235, 248, 248, 248),
          body: Stack(
            alignment: Alignment(0, 0.45),
            children: [
              Container(
                  alignment: Alignment(0, -1),
                  child: Container(
                    width: 350,
                    height: 375,
                    color: Colors.green,
                    alignment: Alignment(0, -0.55),
                    child: Image.asset('assets/img/icPhone.png'),
                  )),
              Container(
                width: 300,
                height: 325,
                decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.green,
                      width: 0.1,
                    ),
                    boxShadow: [
                      new BoxShadow(color: Colors.green, blurRadius: 10)
                    ],
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(25)),
                child: Column(
                  children: [
                    Container(
                      child: Text("Xác minh tài khoản",
                          style: const TextStyle(
                              color: Colors.green,
                              fontSize: 20,
                              fontWeight: FontWeight.bold)),
                      padding: const EdgeInsets.fromLTRB(0, 18, 0, 10),
                    ),
                    Container(
                      child: Column(
                        children: [
                          Text("Nhập số điện thoại của bạn",
                              style: const TextStyle(fontSize: 12),
                              textAlign: TextAlign.center),
                          Text("để xác minh tài khoản của bạn",
                              style: const TextStyle(fontSize: 12),
                              textAlign: TextAlign.center)
                        ],
                      ),
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 80),
                    ),
                    Stack(
                      alignment: Alignment(-0.8, -0.75),
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 22),
                          child: Row(
                            children: [
                              Image.asset("assets/img/icCountry.png", fit: BoxFit.fill, width: 50, height: 40),
                              Icon(Icons.check)
                            ],
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(15, 0, 15, 25),
                          margin:
                              EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                          child: TextField(
                            cursorHeight: 20,
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                              hintText: "nhập số điện thoại",
                              hintStyle: TextStyle(fontSize: 15),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15)),
                                  borderSide: BorderSide(width: 2.0)),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15)),
                                  borderSide: BorderSide(width: 2.0)),
                              // suffixIcon: Icon(Icons.check)
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                        width: 265,
                        child: ListTile(
                          onTap: null,
                          title: Center(
                            child: Text(
                              "Xác nhận",
                              style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.green,
                              width: 0.1,
                            ),
                            color: Color.fromARGB(100, 76, 177, 97),
                            borderRadius: BorderRadius.circular(15)))
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
