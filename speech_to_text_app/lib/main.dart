import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:rxdart/rxdart.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: SpeechToTextDemo(),
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class SpeechToTextDemo extends StatefulWidget {
  @override
  _SpeechToTextDemoState createState() => _SpeechToTextDemoState();
}

class _SpeechToTextDemoState extends State<SpeechToTextDemo> {
  final buttonController = BehaviorSubject<bool>();
  final textController = BehaviorSubject<String>();
  final confidenceController = BehaviorSubject<double>();
  final originalWord = "available";

  String _text = 'Press the button and start speaking';
  double _confidence = 1.0;
  final _speech = stt.SpeechToText();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    buttonController.sink.add(false);
    textController.sink.add(originalWord);
    confidenceController.sink.add(_confidence);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: StreamBuilder(
          stream: confidenceController.stream,
          builder: (context, snapshot) {
            return Text(
                'Confidence: ${(snapshot.data * 100.0).toStringAsFixed(1)}%');
          },
        ),
        centerTitle: true,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: StreamBuilder(
          stream: buttonController.stream,
          builder: (context, snapshot) {
            return AvatarGlow(
              animate: snapshot.data,
              glowColor: Theme.of(context).primaryColor,
              endRadius: 75.0,
              duration: const Duration(milliseconds: 2000),
              repeatPauseDuration: const Duration(milliseconds: 100),
              child: FloatingActionButton(
                onPressed: () async => await _listen(!snapshot.data),
                child: Icon(snapshot.data ? Icons.mic : Icons.mic_none),
              ),
            );
          }),
      body: SingleChildScrollView(
        reverse: true,
        child: Container(
          padding: const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 150.0),
          child: StreamBuilder(
            stream: textController.stream,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: LoadingBouncingLine.circle(
                    size: 100,
                    backgroundColor: Colors.grey,
                  ),
                );
              }
              return Text(
                snapshot.data,
                style: const TextStyle(
                  fontSize: 32.0,
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  _listen(bool snapshot) async {
    if (snapshot) {
      bool available = await _speech.initialize(
        onStatus: (status) {
          print("status: $status");
          if (status == "notListening") buttonController.sink.add(!snapshot);
        },
        onError: (errorNotification) => print("errors: $errorNotification"),
      );
      if (available) {
        // List<LocaleName> locales = await _speech.locales();
        // for(var item in locales){
        //   print(item.localeId);
        // }
        _speech.listen(
          listenMode: stt.ListenMode.confirmation,
          localeId: "en_US",
          onResult: (result) {
            for (var item in result.alternates) {
              if (result.hasConfidenceRating && result.confidence > 0) {
                  if(originalWord == item.recognizedWords){
                    textController.sink.add("matched ${item.confidence*100} %");
                    break;
                  }
              }
            }
          },
        );
      }
    } else {
      await _speech.stop();
    }
    buttonController.sink.add(snapshot);
  }

  /// clearSpace("Hello world Hello") => "HelloworldHello"
  clearSpace(String word) {
    String result = "";
    List<String> words = word.split("");
    for (var item in words) {
      if (item != " ") result += item;
    }
    return result;
  }

  ///
  checkOriginWord(String word) {
    int matched = 0;
    List<String> wordToCheck = word.toLowerCase().split("");
    List<String> original = originalWord.toLowerCase().split("");
    for (int i = 0; i < original.length; i++) {
      if (wordToCheck[i] == original[i])
        matched += 1;
      else {
        break;
      }
    }
    print(matched);
  }
}
